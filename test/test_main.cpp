/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <signal.h>
#include "gtest/gtest.h"
#include "load_service.h"
#include "mock_starter.h"
static void Handle( int, siginfo_t *, void * );
static void Backtrace();
static struct sigaction g_action = {
    .sa_sigaction = Handle,
    .sa_mask = 0,
    .sa_flags = SA_SIGINFO
};
static struct sigaction g_oldAction = {
    .sa_sigaction = nullptr,
    .sa_mask = 0,
    .sa_flags = SA_SIGINFO,
};
int main(int  argc, char *argv[]) 
{
    sigaction(SIGSEGV, &g_action, &g_oldAction);
    sigaction(SIGILL, &g_action, &g_oldAction);
    sigaction(SIGINT, &g_action, &g_oldAction);
    sigaction(SIGABRT, &g_action, &g_oldAction);
    sigaction(SIGTERM, &g_action, &g_oldAction);
    LoadService loadService;
    MockStarter::Instance()->Start();
    std::cout << "start" << std::endl;
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

static void Handle(int sigCode, siginfo_t *info, void *ext)
{
    if (g_oldAction.sa_sigaction != nullptr) {
        g_oldAction.sa_sigaction(sigCode, info, ext);
    }
    Backtrace();
    std::cout << "sigNo:" << info->si_signo << " sigCode:" << info->si_code << std::endl;
}

static void Backtrace()
{
#ifdef __linux__
    size_t size = 100;
    void **callbacks = new void*[size];
    size = backtrace(callbacks, size);
    auto symbols = backtrace_symbols(callbacks, size);
    for (int i = 0; i < size; ++i) {
        if (symbols[i] != nullptr) {
            std::cout << symbols[i] << std::endl;
        }
    }
    free(symbols);
#endif
}