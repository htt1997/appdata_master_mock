/*
* Copyright (c) 2022 Huawei Device Co., Ltd.
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
#define LOG_TAG "CloudDataTest"
#include <gtest/gtest.h>

#include "account/account_delegate.h"
#include "cloud/change_event.h"
#include "cloud/cloud_event.h"
#include "cloud/cloud_server.h"
#include "cloud/schema_meta.h"
#include "communicator/device_manager_adapter.h"
#include "eventcenter/event_center.h"
#include "feature/feature_system.h"
#include "ipc_skeleton.h"
#include "log_print.h"
#include "metadata/meta_data_manager.h"
#include "metadata/store_meta_data.h"
#include "metadata/store_meta_data_local.h"
#include "mock/cloud_server_mock.h"
#include "mock/db_store_mock.h"
#include "mock/general_store_mock.h"
#include "rdb_query.h"
#include "rdb_types.h"
#include "store/auto_cache.h"
#include "store/general_store.h"
using namespace testing::ext;
using namespace OHOS::DistributedData;
using DmAdapter = OHOS::DistributedData::DeviceManagerAdapter;

namespace OHOS::Test {
namespace DistributedDataTest {
class CloudDataTest : public testing::Test {
public:
    static void SetUpTestCase(void);
    static void TearDownTestCase(void);
    void SetUp();
    void TearDown();

    static CloudInfo CLOUD_INFO;
    static SchemaMeta SCHEMA_META;

protected:
    static constexpr uint64_t REMAIN_SPACE = 1000;
    static constexpr uint64_t TOTAL_SPACE = 2000;
    static constexpr const char *TEST_CLOUD_BUNDLE = "test_cloud_bundleName";
    static constexpr const char *TEST_CLOUD_APPID = "test_cloud_appid";
    static constexpr const char *TEST_CLOUD_STORE = "test_cloud_database_name";
    static constexpr const char *TEST_DISTRIBUTED_DATA_BUNDLE = "test_distributeddata";
    static constexpr const char *TEST_DISTRIBUTED_DATA_STORE = "test_service_meta";

    void InitMetaData();
    void InitCloudInfo();
    void InitSchemaMeta();
    StoreMetaData GetStoreMetaData();
    static std::shared_ptr<DBStoreMock> dbStoreMock_;
    StoreMetaData metaData_;
};

std::shared_ptr<DBStoreMock> CloudDataTest::dbStoreMock_ = std::make_shared<DBStoreMock>();
CloudInfo CloudDataTest::CLOUD_INFO;
SchemaMeta CloudDataTest::SCHEMA_META;

void CloudDataTest::InitMetaData()
{
    metaData_.deviceId = DmAdapter::GetInstance().GetLocalDevice().uuid;
    metaData_.appId = TEST_DISTRIBUTED_DATA_BUNDLE;
    metaData_.bundleName = TEST_DISTRIBUTED_DATA_BUNDLE;
    metaData_.tokenId = OHOS::IPCSkeleton::GetCallingTokenID();
    metaData_.user = std::to_string(DistributedData::AccountDelegate::GetInstance()->GetUserByToken(metaData_.tokenId));
    metaData_.area = OHOS::DistributedKv::EL1;
    metaData_.instanceId = 0;
    metaData_.isAutoSync = true;
    metaData_.storeType = 1;
    metaData_.storeId = TEST_DISTRIBUTED_DATA_STORE;
    PolicyValue value;
    value.type = OHOS::DistributedKv::PolicyType::IMMEDIATE_SYNC_ON_ONLINE;
}

void CloudDataTest::InitCloudInfo()
{
    CLOUD_INFO.id = "test_cloud_id";
    CLOUD_INFO.remainSpace = REMAIN_SPACE;
    CLOUD_INFO.totalSpace = TOTAL_SPACE;
    CLOUD_INFO.enableCloud = true;

    CloudInfo::AppInfo appInfo;
    appInfo.bundleName = TEST_CLOUD_BUNDLE;
    appInfo.appId = TEST_CLOUD_APPID;
    appInfo.version = 1;
    appInfo.cloudSwitch = true;

    CLOUD_INFO.apps[TEST_CLOUD_BUNDLE] = std::move(appInfo);
}

void CloudDataTest::InitSchemaMeta()
{
    SchemaMeta::Field field1;
    field1.colName = "test_cloud_field_name1";
    field1.alias = "test_cloud_field_alias1";
    SchemaMeta::Field field2;
    field2.colName = "test_cloud_field_name2";
    field2.alias = "test_cloud_field_alias2";

    SchemaMeta::Table table;
    table.name = "test_cloud_table_name";
    table.alias = "test_cloud_table_alias";
    table.fields.emplace_back(field1);
    table.fields.emplace_back(field2);

    SchemaMeta::Database database;
    database.name = TEST_CLOUD_STORE;
    database.alias = "test_cloud_database_alias";
    database.tables.emplace_back(table);

    SCHEMA_META.version = 1;
    SCHEMA_META.bundleName = TEST_DISTRIBUTED_DATA_BUNDLE;
    SCHEMA_META.databases.emplace_back(database);
}

StoreMetaData CloudDataTest::GetStoreMetaData()
{
    StoreMetaData storeMetaData;
    storeMetaData.deviceId = DmAdapter::GetInstance().GetLocalDevice().uuid;
    storeMetaData.bundleName = TEST_CLOUD_BUNDLE;
    storeMetaData.storeId = TEST_CLOUD_STORE;
    storeMetaData.instanceId = 0;
    storeMetaData.isAutoSync = true;
    storeMetaData.storeType = DistributedRdb::RDB_DEVICE_COLLABORATION;
    storeMetaData.area = OHOS::DistributedKv::EL1;
    storeMetaData.tokenId = OHOS::IPCSkeleton::GetCallingTokenID();
    storeMetaData.user =
        std::to_string(DistributedData::AccountDelegate::GetInstance()->GetUserByToken(storeMetaData.tokenId));
    return storeMetaData;
}

void CloudDataTest::SetUpTestCase(void)
{
    MetaDataManager::GetInstance().Initialize(dbStoreMock_, nullptr, "");

    FeatureSystem::GetInstance().GetCreator("cloud")();
    FeatureSystem::GetInstance().GetCreator("relational_store")();
}

void CloudDataTest::TearDownTestCase() {}

void CloudDataTest::SetUp()
{
    InitMetaData();
    InitCloudInfo();
    InitSchemaMeta();

    MetaDataManager::GetInstance().SaveMeta(metaData_.GetKey(), metaData_);

    auto cloudServerMock = new CloudServerMock(CLOUD_INFO, { { TEST_CLOUD_BUNDLE, SCHEMA_META } });
    CloudServer::RegisterCloudInstance(cloudServerMock);
}

void CloudDataTest::TearDown()
{
    CLOUD_INFO = CloudInfo();
    SCHEMA_META = SchemaMeta();
    if (dbStoreMock_ != nullptr) {
        dbStoreMock_->Reset();
    }
}

/**
* @tc.name: GetSchemaTest001
* @tc.desc: GetSchema from cloud when no schema in meta.
* @tc.type: FUNC
* @tc.require:
* @tc.author: ht
*/
HWTEST_F(CloudDataTest, GetSchemaTest001, TestSize.Level0)
{
    ZLOGI("GetSchemaTest001 start");
    auto cloudInfo = CLOUD_INFO;
    ASSERT_TRUE(MetaDataManager::GetInstance().DelMeta(cloudInfo.GetSchemaKey(TEST_CLOUD_BUNDLE), true));
    SchemaMeta schemaMeta;
    ASSERT_FALSE(MetaDataManager::GetInstance().LoadMeta(cloudInfo.GetSchemaKey(TEST_CLOUD_BUNDLE), schemaMeta, true));
    StoreInfo storeInfo{ IPCSkeleton::GetCallingTokenID(), TEST_CLOUD_BUNDLE, TEST_CLOUD_STORE, 0 };
    auto event = std::make_unique<CloudEvent>(CloudEvent::GET_SCHEMA, std::move(storeInfo));
    EventCenter::GetInstance().PostEvent(std::move(event));
    ASSERT_TRUE(MetaDataManager::GetInstance().LoadMeta(cloudInfo.GetSchemaKey(TEST_CLOUD_BUNDLE), schemaMeta, true));
    ASSERT_EQ(to_string(schemaMeta.Marshall()), to_string(SCHEMA_META.Marshall()));
}

/**
* @tc.name: LocalChangeTest001
* @tc.desc: do cloud sync.
* @tc.type: FUNC
* @tc.require:
* @tc.author: ht
*/
HWTEST_F(CloudDataTest, LocalChangeTest001, TestSize.Level0)
{
    ZLOGI("LocalChangeTest001 start");
    auto cloudInfo = CLOUD_INFO;
    MetaDataManager::GetInstance().SaveMeta(cloudInfo.GetKey(), cloudInfo, true);
    auto schemaMeta = SCHEMA_META;
    auto keys = cloudInfo.GetSchemaKey();
    for (auto &[bundleName, key] : keys) {
        schemaMeta.bundleName = bundleName;
        MetaDataManager::GetInstance().SaveMeta(key, schemaMeta, true);
    }
    auto storeMeta = GetStoreMetaData();
    storeMeta.storeType = DistributedRdb::RDB_DISTRIBUTED_TYPE_MAX;
    MetaDataManager::GetInstance().SaveMeta(storeMeta.GetKey(), storeMeta);

    AutoCache::GetInstance().RegCreator(storeMeta.storeType, [](const StoreMetaData &metaData) -> GeneralStore* {
        return new (std::nothrow) GeneralStoreMock(metaData);
    });
    auto process = [this](const Event &event) {
        auto &evt = static_cast<const CloudEvent &>(event);
        auto storeInfo = evt.GetStoreInfo();
        StoreMetaData meta;
        meta.storeId = storeInfo.storeName;
        meta.bundleName = storeInfo.bundleName;
        meta.user = std::to_string(storeInfo.user);
        meta.instanceId = storeInfo.instanceId;
        meta.deviceId = DmAdapter::GetInstance().GetLocalDevice().uuid;
        ASSERT_TRUE(MetaDataManager::GetInstance().LoadMeta(meta.GetKey(), meta));
        auto store = AutoCache::GetInstance().GetStore(meta, {});
        ASSERT_TRUE(store != nullptr);
    };
    EventCenter::GetInstance().Subscribe(CloudEvent::CLOUD_SYNC, process);
    StoreInfo storeInfo{ IPCSkeleton::GetCallingTokenID(), TEST_CLOUD_BUNDLE, TEST_CLOUD_STORE, 0 };
    auto query = std::make_shared<DistributedRdb::RdbQuery>();
    auto info = ChangeEvent::EventInfo(GeneralStore::CLOUD_TIME_FIRST, 0, false, query, nullptr);
    auto evt = std::make_unique<ChangeEvent>(std::move(storeInfo), std::move(info));
    EventCenter::GetInstance().PostEvent(std::move(evt));
}
} // namespace DistributedDataTest
} // namespace OHOS::Test
