/*
* Copyright (c) 2023 Huawei Device Co., Ltd.
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#include "device_manager_adapter.h"

namespace OHOS {
namespace DistributedData {
//DeviceManagerAdapter &DeviceManagerAdapter::GetInstance()
//{
//    static DeviceManagerAdapter dmAdapter;
//    return dmAdapter;
//}
//void DeviceManagerAdapter::Init(std::shared_ptr<ExecutorPool> executors) {}
////Status DeviceManagerAdapter::StartWatchDeviceChange(const AppDeviceChangeListener *observer, const PipeInfo &pipeInfo)
////{
////
////}
////Status DeviceManagerAdapter::StopWatchDeviceChange(const AppDeviceChangeListener *observer, const PipeInfo &pipeInfo)
////{
////}
////DeviceInfo DeviceManagerAdapter::GetLocalDevice() {}
////std::vector<DeviceInfo> DeviceManagerAdapter::GetRemoteDevices() {}
////DeviceInfo DeviceManagerAdapter::GetDeviceInfo(const std::string &id) {}
//std::string DeviceManagerAdapter::GetUuidByNetworkId(const std::string &networkId)
//{
//    return networkId;
//}
//std::string DeviceManagerAdapter::GetUdidByNetworkId(const std::string &networkId)
//{
//    return networkId;
//}
//std::string DeviceManagerAdapter::CalcClientUuid(const std::string &appId, const std::string &uuid)
//{
//    return appId + uuid;
//}
//std::string DeviceManagerAdapter::ToUUID(const std::string &id)
//{
//    return id;
//}
//std::string DeviceManagerAdapter::ToUDID(const std::string &id)
//{
//    return id;
//}
////static std::vector<std::string> DeviceManagerAdapter::ToUUID(const std::vector<std::string> &devices) {}
////static std::vector<std::string> DeviceManagerAdapter::ToUUID(std::vector<DeviceInfo> devices) {}
//std::string DeviceManagerAdapter::ToNetworkID(const std::string &id)
//{
//    return id;
//}
//void DeviceManagerAdapter::NotifyReadyEvent(const std::string &uuid) {}
//
//bool DeviceManagerAdapter::IsNetworkAvailable()
//{
//    return true;
//}
//
//DeviceManagerAdapter::DeviceManagerAdapter()
//    : cloudDmInfo({ "cloudDeviceId", "cloudDeviceName", 0, "cloudNetworkId", 0 })
//{
//}

} // namespace DistributedData
} // namespace OHOS