/*
* Copyright (c) 2023 Huawei Device Co., Ltd.
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
#include "cloud_server_mock.h"
#include "store/general_value.h"
namespace OHOS {
namespace DistributedData {
CloudServerMock::CloudServerMock(const CloudInfo &cloudInfo, const std::map<std::string,SchemaMeta> &schemaMetas)
    : cloudInfo_(cloudInfo), schemaMetas_(schemaMetas)
{
}

std::pair<int32_t, CloudInfo> CloudServerMock::GetServerInfo(int32_t userId, bool needSpaceInfo)
{
    auto cloudInfo = cloudInfo_;
    cloudInfo.user = userId;
    return { E_OK, cloudInfo };
}

std::pair<int32_t, SchemaMeta> CloudServerMock::GetAppSchema(int32_t userId, const std::string &bundleName)
{
    auto it = schemaMetas_.find(bundleName);
    if (it != schemaMetas_.end()) {
        return { E_OK, it->second };
    }
    return { E_ERROR, SchemaMeta() };
}
} // namespace DistributedData
} // namespace OHOS
