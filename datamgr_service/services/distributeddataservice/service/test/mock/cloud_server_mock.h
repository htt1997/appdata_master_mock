/*
* Copyright (c) 2023 Huawei Device Co., Ltd.
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
#ifndef OHOS_DISTRIBUTEDDATA_SERVICE_TEST_CLOUD_SERVER_MOCK_H
#define OHOS_DISTRIBUTEDDATA_SERVICE_TEST_CLOUD_SERVER_MOCK_H

#include "cloud/cloud_event.h"
#include "cloud/cloud_server.h"
#include "cloud/schema_meta.h"

namespace OHOS {
namespace DistributedData {
class CloudServerMock : public CloudServer {
public:
    CloudServerMock(const CloudInfo& cloudInfo, const std::map<std::string,SchemaMeta>& schemaMetas);
    std::pair<int32_t, CloudInfo> GetServerInfo(int32_t userId, bool needSpaceInfo = true) override;
    std::pair<int32_t, SchemaMeta> GetAppSchema(int32_t userId, const std::string &bundleName) override;
    virtual ~CloudServerMock() = default;

private:
    CloudInfo cloudInfo_;
    std::map<std::string,SchemaMeta> schemaMetas_;
};

} // namespace DistributedData
} // namespace OHOS
#endif //OHOS_DISTRIBUTEDDATA_SERVICE_TEST_CLOUD_SERVER_MOCK_H
