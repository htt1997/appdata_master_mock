@echo off
@echo on
cls
:start
@echo ------------------------------------------------
@echo "Create the distributeddata store dir and config.json"
@echo %date%-%time%
@echo "Input the cygwin install dir like D:\cygwin64"
@set /p cypwin_dir=Please input(full name):
mkdir %cypwin_dir%\system\etc\distributeddata\conf
mkdir %cypwin_dir%\data\service\el1\public\database\distributeddata\meta
mkdir %cypwin_dir%\data\service\el1\public\database\distributeddata\meta\backup
mkdir %cypwin_dir%\data\service\el1\public\database\distributeddata\kvdb
mkdir %cypwin_dir%\data\service\el2\100\SingleStoreImplTest
mkdir %cypwin_dir%\data\test
copy ..\datamgr_service\conf\config.json %cypwin_dir%\system\etc\distributeddata\conf
@echo "SUCCESS"
pause