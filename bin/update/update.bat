@echo off
@echo on
cls
:start
@echo ------------------------------------------------

@echo %date%-%time%
mkdir tmp
@echo "clone the datamgr_service"
git clone https://gitee.com/openharmony/distributeddatamgr_datamgr_service.git ./tmp/datamgr_service
@echo "clone the kv_store"
git clone https://gitee.com/openharmony/distributeddatamgr_kv_store.git ./tmp/kv_store
@echo "clone the relational_store"
git clone https://gitee.com/openharmony/distributeddatamgr_relational_store.git ./tmp/relational_store
@echo "clone the data_object"
git clone https://gitee.com/openharmony/distributeddatamgr_data_object.git ./tmp/data_object
@echo "clone the data_share"
git clone https://gitee.com/openharmony/distributeddatamgr_data_share.git ./tmp/data_share
@echo "clone the preferences"
git clone https://gitee.com/openharmony/distributeddatamgr_preferences.git ./tmp/preferences
@echo "clone the udmf"
git clone https://gitee.com/openharmony/distributeddatamgr_udmf.git ./tmp/udmf

xcopy tmp\* ..\..\ /y /e /i /q

@echo "clone the interface_sdk-js"
git clone https://gitee.com/openharmony/interface_sdk-js.git ./tmp/interface_sdk
xcopy tmp\interface_sdk\api\@ohos.data* ..\..\interface_sdk\api\ /y
xcopy tmp\interface_sdk\api\@ohos.*DataShare* ..\..\interface_sdk\api\ /y
xcopy tmp\interface_sdk\api\data\* ..\..\interface_sdk\api\data\ /y /e
rd /s /q tmp
@echo "FINISHED"
pause