/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "mock.h"

#include "relational_store_client.h"

namespace OHOS {
__attribute__((visibility("default"))) bool PathToRealPath(const std::string &path, std::string &realPath)
{
    realPath = path;
    return true;
}

__attribute__((visibility("default"))) std::string ExtractFilePath(const std::string &fileFullName)
{
    return std::string(fileFullName).substr(0, fileFullName.rfind("/") + 1);
}

namespace NativeRdb {
__attribute__((visibility("default"))) int gettid()
{
    return 0;
}
}
}