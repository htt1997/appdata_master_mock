/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "xcollie.h"

namespace OHOS::HiviewDFX {
XCollie::XCollie() {}
XCollie::~XCollie() {}
void XCollie::CancelTimer(int id) {}
void XCollie::RegisterXCollieChecker(const sptr<XCollieChecker> &checker, unsigned int type) {}
int XCollie::SetTimer(const std::string &name, unsigned int timeout, std::function<void(void *)> func, void *arg,
    unsigned int flag)
{
    return 0;
}
bool XCollie::UpdateTimer(int id, unsigned int timeout)
{
    return false;
}
} // namespace OHOS::HiviewDFX