/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "device_manager.h"
#include "device_manager_impl.h"
#include "softbus_common.h"
namespace OHOS::DistributedHardware {
DeviceManager &DeviceManager::GetInstance()
{
    return DeviceManagerImpl::GetInstance();
}

DeviceManagerImpl &DeviceManagerImpl::GetInstance()
{
    static DeviceManagerImpl instance;
    return instance;
}
int32_t DeviceManagerImpl::InitDeviceManager(const std::string &pkgName, std::shared_ptr<DmInitCallback> dmInitCallback)
{
    return 0;
}
int32_t DeviceManagerImpl::UnInitDeviceManager(const std::string &pkgName)
{
    return 0;
}

int32_t DeviceManagerImpl::GetTrustedDeviceList(const std::string &pkgName, const std::string &extra,
    std::vector<DmDeviceInfo> &deviceList)
{
    DmDeviceInfo deviceInfo;
    GetLocalDeviceInfo(pkgName, deviceInfo);
    for (int i = 0; i < 0; ++i) {
        deviceInfo.networkId[0] = '0' + i;
        deviceInfo.deviceId[0] = '0' + i;
        deviceList.push_back(deviceInfo);
    }
    return 0;
}

int32_t DeviceManagerImpl::GetLocalDeviceInfo(const std::string &pkgName, DmDeviceInfo &deviceInfo)
{
    // (void)strcpy_s(info->networkId, NETWORK_ID_BUF_LEN, "1252645812135842132135452A3B4C5D6E7F");
    (void)strcpy_s(deviceInfo.networkId, DM_MAX_DEVICE_ID_LEN,
        "54a0a92a428005db27c40bad46bf145fede38ec37effe0347cd990fcb031f320");
    (void)strcpy_s(deviceInfo.deviceId, DM_MAX_DEVICE_ID_LEN,
        "54a0a92a428005db27c40bad46bf145fede38ec37effe0347cd990fcb031f320");
    (void)strcpy_s(deviceInfo.deviceName, DM_MAX_DEVICE_ID_LEN, "demo phone");
    deviceInfo.deviceTypeId = SMART_PHONE;
    return 0;
}
int32_t DeviceManagerImpl::RegisterDevStateCallback(const std::string &pkgName, const std::string &extra,
    std::shared_ptr<DeviceStateCallback> callback)
{
    return 0;
}
int32_t DeviceManagerImpl::UnRegisterDevStateCallback(const std::string &pkgName)
{
    return 0;
}
int32_t DeviceManagerImpl::StartDeviceDiscovery(const std::string &pkgName, const DmSubscribeInfo &subscribeInfo,
    const std::string &extra, std::shared_ptr<DiscoveryCallback> callback)
{
    return 0;
}
int32_t DeviceManagerImpl::StopDeviceDiscovery(const std::string &pkgName, uint16_t subscribeId)
{
    return 0;
}
int32_t DeviceManagerImpl::AuthenticateDevice(const std::string &pkgName, int32_t authType,
    const DmDeviceInfo &deviceInfo, const std::string &extra, std::shared_ptr<AuthenticateCallback> callback)
{
    return 0;
}
int32_t DeviceManagerImpl::UnAuthenticateDevice(const std::string &pkgName, const DmDeviceInfo &deviceInfo)
{
    return 0;
}
int32_t DeviceManagerImpl::VerifyAuthentication(const std::string &pkgName, const std::string &authPara,
    std::shared_ptr<VerifyAuthCallback> callback)
{
    return 0;
}

int32_t DeviceManagerImpl::GetFaParam(const std::string &pkgName, DmAuthParam &faParam)
{
    return 0;
}
int32_t DeviceManagerImpl::GetUdidByNetworkId(const std::string &pkgName, const std::string &netWorkId,
    std::string &udid)
{
    if (netWorkId == std::string("no_exist_device_id") || netWorkId.find("invalid_device") != std::string::npos ||
        netWorkId == std::string("1234567890")) {
        return -1;
    }
    udid = netWorkId;
    return 0;
}
int32_t DeviceManagerImpl::GetUuidByNetworkId(const std::string &pkgName, const std::string &netWorkId,
    std::string &uuid)
{
    if (netWorkId == std::string("no_exist_device_id") || netWorkId.find("invalid_device") != std::string::npos ||
        netWorkId == std::string("1234567890")) {
        return -1;
    }
    uuid = netWorkId;
    return 0;
}
int32_t DeviceManagerImpl::RegisterDevStateCallback(const std::string &pkgName, const std::string &extra)
{
    return 0;
}
int32_t DeviceManagerImpl::UnRegisterDevStateCallback(const std::string &pkgName, const std::string &extra)
{
    return 0;
}
int32_t DeviceManagerImpl::RequestCredential(const std::string &pkgName, const std::string &reqJsonStr,
    std::string &returnJsonStr)
{
    return 0;
}
int32_t DeviceManagerImpl::ImportCredential(const std::string &pkgName, const std::string &credentialInfo)
{
    return 0;
}
int32_t DeviceManagerImpl::DeleteCredential(const std::string &pkgName, const std::string &deleteInfo)
{
    return 0;
}
int32_t DeviceManagerImpl::RegisterCredentialCallback(const std::string &pkgName,
    std::shared_ptr<CredentialCallback> callback)
{
    return 0;
}
int32_t DeviceManagerImpl::UnRegisterCredentialCallback(const std::string &pkgName)
{
    return 0;
}
int32_t DeviceManagerImpl::PublishDeviceDiscovery(const std::string &pkgName, const DmPublishInfo &publishInfo,
    std::shared_ptr<PublishCallback> callback)
{
    return 0;
}
int32_t DeviceManagerImpl::UnPublishDeviceDiscovery(const std::string &pkgName, int32_t publishId)
{
    return 0;
}
int32_t DeviceManagerImpl::NotifyEvent(const std::string &pkgName, const int32_t eventId, const std::string &event)
{
    return 0;
}
int32_t DeviceManagerImpl::GetDeviceInfo(const std::string &pkgName, const std::string networkId,
    DmDeviceInfo &deviceInfo)
{
    return 0;
}
int32_t DeviceManagerImpl::SetUserOperation(const std::string &pkgName, int32_t action, const std::string &params)
{
    return 0;
}

int32_t DeviceManagerImpl::GetEncryptedUuidByNetworkId(const std::string &pkgName, const std::string &networkId,
    std::string &uuid)
{
    if (pkgName.empty() || networkId.empty() || networkId == std::string("no_exist_device_id") ||
        networkId.find("invalid_device") != std::string::npos || networkId == std::string("1234567890")) {
        return -1;
    }
    uuid = networkId;
    return 0;
}

int32_t DeviceManagerImpl::GenerateEncryptedUuid(const std::string &pkgName, const std::string &uuid,
    const std::string &appId, std::string &encryptedUuid)
{
    if (pkgName.empty() || uuid.empty()) {
        return -1;
    }
    encryptedUuid = uuid;
    return 0;
}

bool DeviceManagerImpl::IsSameAccount(const std::string &netWorkId)
{
    return false;
}
int32_t DeviceManagerImpl::GetTrustedDeviceList(const std::string &pkgName, const std::string &extra, bool isRefresh,
    std::vector<DmDeviceInfo> &deviceList)
{
    return 0;
}
int32_t DeviceManagerImpl::GetAvailableDeviceList(const std::string &pkgName,
    std::vector<DmDeviceBasicInfo> &deviceList)
{
    return 0;
}
int32_t DeviceManagerImpl::RegisterDevStatusCallback(const std::string &pkgName, const std::string &extra,
    std::shared_ptr<DeviceStatusCallback> callback)
{
    return 0;
}
int32_t DeviceManagerImpl::UnRegisterDevStatusCallback(const std::string &pkgName)
{
    return 0;
}
int32_t DeviceManagerImpl::StartDeviceDiscovery(const std::string &pkgName, uint64_t tokenId,
    const std::string &filterOptions, std::shared_ptr<DiscoveryCallback> callback)
{
    return 0;
}
int32_t DeviceManagerImpl::StopDeviceDiscovery(uint64_t tokenId, const std::string &pkgName)
{
    return 0;
}
int32_t DeviceManagerImpl::RegisterDeviceManagerFaCallback(const std::string &pkgName,
    std::shared_ptr<DeviceManagerUiCallback> callback)
{
    return 0;
}
int32_t DeviceManagerImpl::UnRegisterDeviceManagerFaCallback(const std::string &pkgName)
{
    return 0;
}
int32_t DeviceManagerImpl::RegisterPinHolderCallback(const std::string &pkgName,
    std::shared_ptr<PinHolderCallback> callback)
{
    return 0;
}
int32_t DeviceManagerImpl::CreatePinHolder(const std::string &pkgName, const PeerTargetId &targetId, DmPinType pinType,
    const std::string &payload)
{
    return 0;
}
int32_t DeviceManagerImpl::DestroyPinHolder(const std::string &pkgName, const PeerTargetId &targetId,
    DmPinType pinType, const std::string &payload)
{
    return 0;
}
int32_t DeviceManagerImpl::RequestCredential(const std::string &pkgName, std::string &returnJsonStr)
{
    return 0;
}
int32_t DeviceManagerImpl::CheckCredential(const std::string &pkgName, const std::string &reqJsonStr,
    std::string &returnJsonStr)
{
    return 0;
}
int32_t DeviceManagerImpl::ImportCredential(const std::string &pkgName, const std::string &reqJsonStr,
    std::string &returnJsonStr)
{
    return 0;
}
int32_t DeviceManagerImpl::DeleteCredential(const std::string &pkgName, const std::string &reqJsonStr,
    std::string &returnJsonStr)
{
    return 0;
}
int32_t DeviceManagerImpl::CheckAPIAccessPermission()
{
    return 0;
}
int32_t DeviceManagerImpl::CheckNewAPIAccessPermission()
{
    return 0;
}
int32_t DeviceManagerImpl::OnDmServiceDied()
{
    return 0;
}
int32_t DeviceManagerImpl::RegisterUiStateCallback(const std::string &pkgName)
{
    return 0;
}
int32_t DeviceManagerImpl::UnRegisterUiStateCallback(const std::string &pkgName)
{
    return 0;
}
int32_t DeviceManagerImpl::GetLocalDeviceNetWorkId(const std::string &pkgName, std::string &networkId)
{
    return 0;
}
int32_t DeviceManagerImpl::GetLocalDeviceId(const std::string &pkgName, std::string &networkId)
{
    return 0;
}
int32_t DeviceManagerImpl::GetLocalDeviceType(const std::string &pkgName, int32_t &deviceType)
{
    return 0;
}
int32_t DeviceManagerImpl::GetLocalDeviceName(const std::string &pkgName, std::string &deviceName)
{
    return 0;
}
int32_t DeviceManagerImpl::GetDeviceName(const std::string &pkgName, const std::string &networkId,
    std::string &deviceName)
{
    return 0;
}
int32_t DeviceManagerImpl::GetDeviceType(const std::string &pkgName, const std::string &networkId, int32_t &deviceType)
{
    return 0;
}
int32_t DeviceManagerImpl::BindDevice(const std::string &pkgName, int32_t bindType, const std::string &deviceId,
    const std::string &extra, std::shared_ptr<AuthenticateCallback> callback)
{
    return 0;
}
int32_t DeviceManagerImpl::UnBindDevice(const std::string &pkgName, const std::string &deviceId)
{
    return 0;
}
int32_t DeviceManagerImpl::GetNetworkTypeByNetworkId(const std::string &pkgName, const std::string &netWorkId,
    int32_t &netWorkType)
{
    return 0;
}
int32_t DeviceManagerImpl::ImportAuthCode(const std::string &pkgName, const std::string &authCode)
{
    return 0;
}
int32_t DeviceManagerImpl::ExportAuthCode(std::string &authCode)
{
    return 0;
}
int32_t DeviceManagerImpl::StartDiscovering(const std::string &pkgName,
    std::map<std::string, std::string> &discoverParam, const std::map<std::string, std::string> &filterOptions,
    std::shared_ptr<DiscoveryCallback> callback)
{
    return 0;
}
int32_t DeviceManagerImpl::StopDiscovering(const std::string &pkgName,
    std::map<std::string, std::string> &discoverParam)
{
    return 0;
}
int32_t DeviceManagerImpl::RegisterDiscoveryCallback(const std::string &pkgName,
    std::map<std::string, std::string> &discoverParam, const std::map<std::string, std::string> &filterOptions,
    std::shared_ptr<DiscoveryCallback> callback)
{
    return 0;
}
int32_t DeviceManagerImpl::UnRegisterDiscoveryCallback(const std::string &pkgName)
{
    return 0;
}
int32_t DeviceManagerImpl::StartAdvertising(const std::string &pkgName,
    std::map<std::string, std::string> &advertiseParam, std::shared_ptr<PublishCallback> callback)
{
    return 0;
}
int32_t DeviceManagerImpl::StopAdvertising(const std::string &pkgName,
    std::map<std::string, std::string> &advertiseParam)
{
    return 0;
}
int32_t DeviceManagerImpl::BindTarget(const std::string &pkgName, const PeerTargetId &targetId,
    std::map<std::string, std::string> &bindParam, std::shared_ptr<BindTargetCallback> callback)
{
    return 0;
}
int32_t DeviceManagerImpl::UnbindTarget(const std::string &pkgName, const PeerTargetId &targetId,
    std::map<std::string, std::string> &unbindParam, std::shared_ptr<UnbindTargetCallback> callback)
{
    return 0;
}
int32_t DeviceManagerImpl::GetTrustedDeviceList(const std::string &pkgName,
    const std::map<std::string, std::string> &filterOptions, bool isRefresh, std::vector<DmDeviceInfo> &deviceList)
{
    return 0;
}
int32_t DeviceManagerImpl::RegisterDevStateCallback(const std::string &pkgName,
    const std::map<std::string, std::string> &extraParam, std::shared_ptr<DeviceStateCallback> callback)
{
    return 0;
}
int32_t DeviceManagerImpl::CheckAccessToTarget(uint64_t tokenId, const std::string &targetId)
{
    return 0;
}
int32_t DeviceManagerImpl::DpAclAdd(const int64_t accessControlId, const std::string &udid, const int32_t bindType)
{
    return 0;
}
int32_t DeviceManagerImpl::GetDeviceSecurityLevel(const std::string &pkgName, const std::string &networkId,
    int32_t &securityLevel)
{
    return 0;
}
bool DeviceManagerImpl::CheckAccessControl(const DmAccessCaller &caller, const DmAccessCallee &callee)
{
    return false;
}
bool DeviceManagerImpl::CheckIsSameAccount(const DmAccessCaller &caller, const DmAccessCallee &callee)
{
    return false;
}
int32_t DeviceManagerImpl::GetErrCode(int32_t errCode)
{
    return 0;
}
int32_t DeviceManagerImpl::ShiftLNNGear(const std::string &pkgName)
{
    return 0;
}
int32_t DeviceManagerImpl::RegDevTrustChangeCallback(const std::string &pkgName,
    std::shared_ptr<DevTrustChangeCallback> callback)
{
    return 0;
}
int32_t DeviceManagerImpl::SetDnPolicy(const std::string &pkgName, std::map<std::string, std::string> &policy)
{
    return 0;
}
int32_t DeviceManagerImpl::RegisterDeviceScreenStatusCallback(const std::string &pkgName,
    std::shared_ptr<DeviceScreenStatusCallback> callback)
{
    return 0;
}
int32_t DeviceManagerImpl::UnRegisterDeviceScreenStatusCallback(const std::string &pkgName)
{
    return 0;
}
int32_t DeviceManagerImpl::GetDeviceScreenStatus(const std::string &pkgName, const std::string &networkId,
    int32_t &screenStatus)
{
    return 0;
}
int32_t DeviceManagerImpl::StopAuthenticateDevice(const std::string &pkgName)
{
    return 0;
}
int32_t DeviceManagerImpl::GetNetworkIdByUdid(const std::string &pkgName, const std::string &udid,
    std::string &networkId)
{
    return 0;
}
int32_t DeviceManagerImpl::RegisterCredentialAuthStatusCallback(const std::string &pkgName,
    std::shared_ptr<CredentialAuthStatusCallback> callback)
{
    return 0;
}
int32_t DeviceManagerImpl::UnRegisterCredentialAuthStatusCallback(const std::string &pkgName)
{
    return 0;
}

} // namespace OHOS::DistributedHardware