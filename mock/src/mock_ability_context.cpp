/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "ability/native/ability_context.h"
#include "context_container.h"
namespace OHOS::AppExecFwk {
void AbilityContext::AttachBaseContext(const std::shared_ptr<Context> &base) {}
std::string AbilityContext::GetExternalCacheDir() { return ""; }
std::string AbilityContext::GetExternalFilesDir(string &type) { return ""; }
std::string AbilityContext::GetFilesDir() { return ""; }
bool AbilityContext::IsUpdatingConfigurations() { return true; }
bool AbilityContext::PrintDrawnCompleted() { return true; }
std::string AbilityContext::GetNoBackupFilesDir() { return ""; }
void AbilityContext::UnauthUriPermission(const string &permission, const Uri &uri, int uid) {}
std::string AbilityContext::GetDistributedDir() { return ""; }
void AbilityContext::SetPattern(int patternId) {}
std::shared_ptr<Context> AbilityContext::GetAbilityPackageContext() { return nullptr; }
std::string AbilityContext::GetProcessName() { return "mock"; }
void AbilityContext::InitResourceManager(BundleInfo &bundleInfo, std::shared_ptr<ContextDeal> &deal) {}
ErrCode AbilityContext::StartAbility(const Want &Want, int requestCode) { return 0; }
ErrCode AbilityContext::StartAbility(const Want &want, int requestCode, const AbilityStartSetting &abilityStartSetting)
{
    return 0;
}
ErrCode AbilityContext::TerminateAbility(int requestCode) { return 0; }
ErrCode AbilityContext::TerminateAbility() { return 0; }
std::string AbilityContext::GetCallingBundle() { return ""; }
std::shared_ptr<ElementName> AbilityContext::GetElementName() { return std::shared_ptr<ElementName>(); }
std::shared_ptr<ElementName> AbilityContext::GetCallingAbility() { return std::shared_ptr<ElementName>(); }
bool AbilityContext::ConnectAbility(const Want &want, const sptr<AAFwk::IAbilityConnection> &conn) { return false; }
ErrCode AbilityContext::DisconnectAbility(const sptr<AAFwk::IAbilityConnection> &conn) { return 0; }
bool AbilityContext::StopAbility(const Want &want) { return false; }
std::shared_ptr<ApplicationInfo> AbilityContext::GetApplicationInfo() const { return nullptr; }
std::string AbilityContext::GetCacheDir() { return ""; }
std::string AbilityContext::GetCodeCacheDir() { return ""; }
std::string AbilityContext::GetDatabaseDir() { return ""; }
std::string AbilityContext::GetDataDir() { return ""; }
std::string AbilityContext::GetDir(const string &name, int mode) { return ""; }
sptr<IBundleMgr> AbilityContext::GetBundleManager() const { return nullptr; }
std::string AbilityContext::GetBundleCodePath() { return ""; }
std::string AbilityContext::GetBundleName() const { return ""; }
std::string AbilityContext::GetBundleResourcePath() { return ""; }
std::shared_ptr<Context> AbilityContext::GetApplicationContext() const { return nullptr; }
std::shared_ptr<Context> AbilityContext::GetContext() { return nullptr; }
sptr<AAFwk::IAbilityManager> AbilityContext::GetAbilityManager() { return nullptr; }
std::shared_ptr<ProcessInfo> AbilityContext::GetProcessInfo() const { return nullptr; }
std::string AbilityContext::GetAppType() { return ""; }
const std::shared_ptr<AbilityInfo> AbilityContext::GetAbilityInfo() { return nullptr; }
std::shared_ptr<HapModuleInfo> AbilityContext::GetHapModuleInfo() { return nullptr; }
std::shared_ptr<Context> AbilityContext::CreateBundleContext(std::string bundleName, int flag, int accountId)
{
    return nullptr;
}
std::shared_ptr<Global::Resource::ResourceManager> AbilityContext::GetResourceManager() const { return nullptr; }
int AbilityContext::VerifyPermission(const string &permission, int pid, int uid) { return 0; }
void AbilityContext::RequestPermissionsFromUser(std::vector<std::string> &permissions,
                                                std::vector<int> &permissionsState, int requestCode) {}
bool AbilityContext::DeleteFile(const string &fileName) { return true; }
void AbilityContext::SetCallingContext(const string &deviceId, const string &bundleName, const string &abilityName,
                                       const string &moduleName) {}
Uri AbilityContext::GetCaller() { return Uri(""); }
std::string AbilityContext::GetString(int resId) { return ""; }
std::vector<std::string> AbilityContext::GetStringArray(int resId) { return {}; }
std::vector<int> AbilityContext::GetIntArray(int resId) { return {}; }
std::map<std::string, std::string> AbilityContext::GetTheme() { return {}; }
void AbilityContext::SetTheme(int themeId) {}
std::map<std::string, std::string> AbilityContext::GetPattern() { return {}; }
int AbilityContext::GetColor(int resId) { return 0; }
int AbilityContext::GetThemeId() { return 1; }
bool AbilityContext::TerminateAbilityResult(int startId) { return false; }
int AbilityContext::GetDisplayOrientation() { return 0; }
std::string AbilityContext::GetPreferencesDir() { return {}; }
void AbilityContext::SetColorMode(int mode) {}
int AbilityContext::GetColorMode() { return 0; }
int AbilityContext::GetMissionId() { return 0; }
void AbilityContext::StartAbilities(const std::vector<AAFwk::Want> &wants) {}
sptr<IRemoteObject> AbilityContext::GetToken() { return sptr<IRemoteObject>(); }
AppExecFwk::AbilityType AbilityContext::GetAbilityInfoType() { return AbilityType::EXTENSION; }
void AbilityContext::GetPermissionDes(const string &permissionName, string &des) {}
}