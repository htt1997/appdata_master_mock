/*
* Copyright (c) 2021 Huawei Device Co., Ltd.
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
#include "ohos_account_kits.h"
#include "os_account_subscribe_info.h"
#include "os_account_manager.h"
#include "account_info.h"
namespace OHOS::AccountSA {
OsAccountSubscribeInfo::OsAccountSubscribeInfo()
{}
OsAccountSubscribeInfo::OsAccountSubscribeInfo(const std::set<OsAccountState> &states, bool withHandshake) {}
OsAccountSubscribeInfo::OsAccountSubscribeInfo(const OS_ACCOUNT_SUBSCRIBE_TYPE &osAccountSubscribeType,
    const std::string &name)
{
}
OsAccountSubscribeInfo::~OsAccountSubscribeInfo() {}
void OsAccountSubscribeInfo::GetOsAccountSubscribeType(OS_ACCOUNT_SUBSCRIBE_TYPE &osAccountSubscribeType) const {}
void OsAccountSubscribeInfo::SetOsAccountSubscribeType(const OS_ACCOUNT_SUBSCRIBE_TYPE &osAccountSubscribeType) {}
void OsAccountSubscribeInfo::GetName(std::string &name) const {}
void OsAccountSubscribeInfo::SetName(const std::string &name) {}
void OsAccountSubscribeInfo::GetStates(std::set<OsAccountState> &states) const {}
bool OsAccountSubscribeInfo::IsWithHandshake() const
{
    return false;
}
bool OsAccountSubscribeInfo::Marshalling(Parcel &parcel) const
{
    return false;
}
OsAccountSubscribeInfo *OsAccountSubscribeInfo::Unmarshalling(Parcel &parcel)
{
    return nullptr;
}
class OhosAccountKitsImpl : public OHOS::AccountSA::OhosAccountKits {
public:
    std::pair<bool, OHOS::AccountSA::OhosAccountInfo> QueryOhosAccountInfo() override;
    bool UpdateOhosAccountInfo(const std::string& accountName, const std::string& uid,
        const std::string& eventStr) override;
    int32_t QueryDeviceAccountId(std::int32_t &accountId) override;
    int32_t GetDeviceAccountIdByUID(int32_t& uid) override;

    int32_t GetOhosAccountInfoByUserId(int32_t userId, OhosAccountInfo &accountInfo) override;

    int32_t GetOsAccountDistributedInfo(int32_t localId, OhosAccountInfo &accountInfo) override;
    std::pair<bool, OHOS::AccountSA::OhosAccountInfo> QueryOhosAccountInfoByUserId(std::int32_t userId) override;
    ~OhosAccountKitsImpl() override = default;
    int32_t GetOhosAccountInfo(OhosAccountInfo &accountInfo) override;
    int32_t SetOhosAccountInfo(const OhosAccountInfo &ohosAccountInfo, const std::string &eventStr) override;
    int32_t SetOhosAccountInfoByUserId(const int32_t userId, const OhosAccountInfo &ohosAccountInfo,
        const std::string &eventStr) override;
};
OhosAccountKits& OhosAccountKits::GetInstance()
{
    static OhosAccountKitsImpl instance;
    return instance;
}
ErrCode OsAccountManager::GetOsAccountLocalIdFromUid(const int uid, int& id)
{
    int32_t localUid = uid;
    id = OhosAccountKits::GetInstance().GetDeviceAccountIdByUID(localUid);
    return ERR_OK;
}

ErrCode OsAccountManager::QueryActiveOsAccountIds(std::vector<int32_t>& ids)
{
    ids = { 0, 100, 101 };
    return ERR_OK;
}

ErrCode OsAccountManager::IsOsAccountActived(const int id, bool& isOsAccountActived)
{
    isOsAccountActived = (id == 0 || id == 100);
    return ERR_OK;
}
ErrCode OsAccountManager::IsOsAccountVerified(const int id, bool& isVerified)
{
    return ERR_OK;
}
ErrCode OsAccountManager::GetForegroundOsAccounts(std::vector<ForegroundOsAccount>& accounts)
{
    accounts = { { 100, 0 } };
    return 0;
}
ErrCode OsAccountManager::GetForegroundOsAccountLocalId(int32_t &localId)
{
    return 0;
}
ErrCode OsAccountManager::IsOsAccountDeactivating(const int id, bool &isVerified)
{
    return 0;
}
ErrCode OsAccountManager::SubscribeOsAccount(const std::shared_ptr<OsAccountSubscriber> &subscriber)
{
    return 0;
}
ErrCode OsAccountManager::UnsubscribeOsAccount(const std::shared_ptr<OsAccountSubscriber> &subscriber)
{
    return 0;
}
std::pair<bool, OHOS::AccountSA::OhosAccountInfo> OhosAccountKitsImpl::QueryOhosAccountInfo()
{
    return std::pair<bool, OHOS::AccountSA::OhosAccountInfo>();
}
bool OhosAccountKitsImpl::UpdateOhosAccountInfo(const std::string& accountName, const std::string& uid,
    const std::string& eventStr)
{
    return false;
}
int32_t OhosAccountKitsImpl::GetDeviceAccountIdByUID(int32_t& uid)
{
    return 0;
}

std::pair<bool, OhosAccountInfo> OhosAccountKitsImpl::QueryOhosAccountInfoByUserId(std::int32_t userId)
{
    return std::pair<bool, OHOS::AccountSA::OhosAccountInfo>();
}
ErrCode OhosAccountKitsImpl::GetOhosAccountInfo(OhosAccountInfo &accountInfo)
{
    return 0;
}
int32_t OhosAccountKitsImpl::SetOhosAccountInfo(const OhosAccountInfo &ohosAccountInfo, const std::string &eventStr)
{
    return 0;
}
ErrCode OhosAccountKitsImpl::SetOhosAccountInfoByUserId(const int32_t userId, const OhosAccountInfo &ohosAccountInfo,
    const std::string &eventStr)
{
    return 0;
}
int32_t OhosAccountKitsImpl::QueryDeviceAccountId(int32_t &accountId)
{
    return 0;
}
int32_t OhosAccountKitsImpl::GetOhosAccountInfoByUserId(int32_t userId, OhosAccountInfo &accountInfo)
{
    return 0;
}
int32_t OhosAccountKitsImpl::GetOsAccountDistributedInfo(int32_t localId, OhosAccountInfo &accountInfo)
{
    return 0;
}
} // namespace OHOS::AccountSA
