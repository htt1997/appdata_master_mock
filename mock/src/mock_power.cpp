/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <power_mgr_client.h>
#include <battery_srv_client.h>
namespace OHOS {
namespace PowerMgr {
BatterySrvClient::BatterySrvClient() {}
BatterySrvClient::~BatterySrvClient() {}
int32_t BatterySrvClient::GetCapacity() { return 0; }
BatteryChargeState BatterySrvClient::GetChargingStatus() { return BatteryChargeState::CHARGE_STATE_DISABLE; }
BatteryHealthState BatterySrvClient::GetHealthStatus() { return BatteryHealthState::HEALTH_STATE_GOOD; }
BatteryPluggedType BatterySrvClient::GetPluggedType() { return BatteryPluggedType::PLUGGED_TYPE_AC; }
int32_t BatterySrvClient::GetVoltage() { return 0; }
bool BatterySrvClient::GetPresent() { return false; }
std::string BatterySrvClient::GetTechnology() { return std::string(); }
int32_t BatterySrvClient::GetBatteryTemperature() { return 0; }
ErrCode BatterySrvClient::Connect() { return 0; }
void BatterySrvClient::ResetProxy(const wptr<IRemoteObject> &remote) {}

PowerMgrClient::PowerMgrClient() {}
PowerMgrClient::~PowerMgrClient(){}
void PowerMgrClient::RebootDevice(const std::string &reason){}
void PowerMgrClient::ShutDownDevice(const std::string &reason){}
void PowerMgrClient::SuspendDevice(SuspendDeviceType reason, bool suspendImmed){}
void PowerMgrClient::WakeupDevice(WakeupDeviceType reason, const std::string &detail){}
void PowerMgrClient::RefreshActivity(UserActivityType type){}
bool PowerMgrClient::IsScreenOn(){    return false;}
PowerState PowerMgrClient::GetState(){    return PowerState::SLEEP;}
bool PowerMgrClient::ForceSuspendDevice(){    return false;}
bool PowerMgrClient::IsRunningLockTypeSupported(uint32_t type)
{
    return false;
}
void PowerMgrClient::SetDisplaySuspend(bool enable)
{

}
void PowerMgrClient::SetDeviceMode(const uint32_t mode)
{

}
uint32_t PowerMgrClient::GetDeviceMode()
{
    return 0;
}
std::shared_ptr<RunningLock> PowerMgrClient::CreateRunningLock(const std::string &name, RunningLockType type)
{
    return std::shared_ptr<RunningLock>();
}
void PowerMgrClient::RegisterPowerStateCallback(const sptr<IPowerStateCallback> &callback)
{

}
void PowerMgrClient::UnRegisterPowerStateCallback(const sptr<IPowerStateCallback> &callback)
{

}
void PowerMgrClient::UnRegisterShutdownCallback(const sptr<IShutdownCallback> &callback)
{

}
void PowerMgrClient::RegisterPowerModeCallback(const sptr<IPowerModeCallback> &callback)
{

}
void PowerMgrClient::UnRegisterPowerModeCallback(const sptr<IPowerModeCallback> &callback)
{

}
ErrCode PowerMgrClient::Connect()
{
    return 0;
}
void PowerMgrClient::ResetProxy(const wptr<IRemoteObject> &remote)
{

}
void PowerMgrClient::RegisterShutdownCallback(const sptr<IShutdownCallback> &callback,
                                              IShutdownCallback::ShutdownPriority priority)
{

}
std::string PowerMgrClient::Dump(const std::vector<std::string> &args)
{
    return std::string();
}
}

}

