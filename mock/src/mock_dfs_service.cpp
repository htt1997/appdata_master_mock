#include "cloud_sync_asset_manager.h"
#include "cloud_sync_asset_manager_impl.h"
#include "copy/file_copy_manager.h"
#include "copy/file_size_utils.h"

namespace OHOS {
namespace FileManagement::CloudSync {

CloudSyncAssetManager &CloudSyncAssetManager::GetInstance()
{
    return CloudSyncAssetManagerImpl::GetInstance();
}

CloudSyncAssetManagerImpl &CloudSync::CloudSyncAssetManagerImpl::GetInstance()
{
    static CloudSyncAssetManagerImpl cloudSyncAssetManager;
    return cloudSyncAssetManager;
}

int32_t CloudSyncAssetManagerImpl::UploadAsset(const int32_t userId, const std::string &request, std::string &result)
{
    return 0;
}
int32_t CloudSyncAssetManagerImpl::DownloadFile(const int32_t userId, const std::string &bundleName,
    AssetInfo &assetInfo)
{
    return 0;
}
int32_t CloudSyncAssetManagerImpl::DeleteAsset(const int32_t userId, const std::string &uri)
{
    return 0;
}
int32_t CloudSyncAssetManagerImpl::DownloadFile(const int32_t userId, const std::string &bundleName,
    const std::string &networkId, AssetInfo &assetInfo, CloudSyncAssetManager::ResultCallback resultCallback)
{
    return 0;
}
} // namespace FileManagement::CloudSync
namespace Storage::DistributedFile {
std::shared_ptr<FileCopyManager> FileCopyManager::GetInstance()
{
    static std::shared_ptr<FileCopyManager> Manger = std::make_shared<FileCopyManager>();
    return Manger;
}
int32_t FileCopyManager::Copy(const std::string &srcUri, const std::string &destUri, ProcessCallback &processCallback)
{
    return 0;
}
int32_t FileCopyManager::Cancel(const std::string &srcUri, const std::string &destUri)
{
    return 0;
}
int32_t FileCopyManager::Cancel()
{
    return 0;
}
void FileSizeUtils::Deleter(struct NameList *arg)
{}
int32_t FileSizeUtils::GetSize(const std::string &uri, bool isSrcUri, uint64_t &size)
{
    return 0;
}
int32_t FileSizeUtils::IsDirectory(const std::string &uri, bool isSrcUri, bool &isDirectory)
{
    return 0;
}
std::string FileSizeUtils::GetPathFromUri(const std::string &uri, bool isSrcUri)
{
    return std::string();
}
int32_t FileSizeUtils::GetFileSize(const std::string &path, uint64_t &size)
{
    return 0;
}
int32_t FileSizeUtils::GetDirSize(const std::string &path, uint64_t &size)
{
    return 0;
}
std::unique_ptr<struct NameList, void (*)(NameList *)> FileSizeUtils::GetDirNameList(const std::string &path)
{
    NameList *list = new NameList;
    return std::unique_ptr<struct NameList, void (*)(NameList *)>(list, Deleter);
}
}
} // namespace OHOS