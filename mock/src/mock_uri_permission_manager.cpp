/*
* Copyright (c) 2022-2023 Huawei Device Co., Ltd.
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#include "system_ability_definition.h"
#include "uri_permission_manager_client.h"

namespace OHOS {
namespace AAFwk {
UriPermissionManagerClient& UriPermissionManagerClient::GetInstance()
{
    static UriPermissionManagerClient instance;
    return instance;
}

int UriPermissionManagerClient::GrantUriPermission(const std::vector<Uri>& uriVec, unsigned int flag,
    const std::string targetBundleName, int32_t appIndex, uint32_t initiatorTokenId)
{
    return 0;
}

void UriPermissionManagerClient::RevokeUriPermission(const Security::AccessToken::AccessTokenID tokenId) {}

sptr<IUriPermissionManager> UriPermissionManagerClient::ConnectUriPermService()
{
    return nullptr;
}

bool UriPermissionManagerClient::LoadUriPermService()
{
    return true;
}

sptr<IUriPermissionManager> UriPermissionManagerClient::GetUriPermMgr()
{
    return nullptr;
}

void UriPermissionManagerClient::SetUriPermMgr(const sptr<IRemoteObject>& remoteObject) {}

void UriPermissionManagerClient::OnLoadSystemAbilitySuccess(const sptr<IRemoteObject>& remoteObject) {}

void UriPermissionManagerClient::OnLoadSystemAbilityFail() {}

void UriPermissionManagerClient::ClearProxy() {}
int UriPermissionManagerClient::GrantUriPermission(const Uri& uri, unsigned int flag,
    const std::string targetBundleName, int32_t appIndex, uint32_t initiatorTokenId)
{
    return 0;
}
int32_t UriPermissionManagerClient::GrantUriPermissionPrivileged(const std::vector<Uri>& uriVec, uint32_t flag,
    const std::string& targetBundleName, int32_t appIndex)
{
    return 0;
}
int UriPermissionManagerClient::GrantUriPermissionFor2In1(const std::vector<Uri>& uriVec, unsigned int flag,
    const std::string& targetBundleName, int32_t appIndex, bool isSystemAppCall)
{
    return 0;
}
int UriPermissionManagerClient::RevokeAllUriPermissions(const Security::AccessToken::AccessTokenID tokenId)
{
    return 0;
}
bool UriPermissionManagerClient::VerifyUriPermission(const Uri& uri, uint32_t flag, uint32_t tokenId)
{
    return false;
}
std::vector<bool> UriPermissionManagerClient::CheckUriAuthorization(const std::vector<std::string>& uriVec,
    uint32_t flag, uint32_t tokenId)
{
    return std::vector<bool>();
}
bool UriPermissionManagerClient::IsAuthorizationUriAllowed(uint32_t fromTokenId)
{
    return false;
}

int UriPermissionManagerClient::RevokeUriPermissionManually(const Uri &uri, const std::string bundleName,
    int32_t instance)
{
    return 0;
}

void UriPermissionManagerClient::UpmsDeathRecipient::OnRemoteDied([[maybe_unused]] const wptr<IRemoteObject>& remote)
{
}
} // namespace AAFwk
} // namespace OHOS