#include "net_conn_callback_stub.h"
#include "net_conn_client.h"
#include "net_handle.h"

namespace OHOS{
namespace NetManagerStandard{

int32_t NetHandle::BindSocket(int32_t socket_fd)
{
    return 0;
}
int32_t NetHandle::GetAddressesByName(const std::string &host,
    std::vector<INetAddr> &addrList)
{
    return 0;
}
int32_t NetHandle::GetAddressByName(const std::string &host,
    OHOS::NetManagerStandard::INetAddr &addr)
{
    return 0;
}

NetConnClient &NetConnClient::GetInstance()
{
    static NetConnClient netConnClient;
    return netConnClient;
}

int32_t NetConnClient::SystemReady()
{
    return 0;
}
int32_t NetConnClient::SetInternetPermission(uint32_t uid, uint8_t allow)
{
    return 0;
}
int32_t NetConnClient::RegisterNetSupplier(NetBearType bearerType, const std::string &ident,
    const std::set<NetCap> &netCaps, uint32_t &supplierId)
{
    return 0;
}
int32_t NetConnClient::UnregisterNetSupplier(uint32_t supplierId)
{
    return 0;
}
int32_t NetConnClient::RegisterNetSupplierCallback(uint32_t supplierId, const sptr<NetSupplierCallbackBase> &callback)
{
    return 0;
}
int32_t NetConnClient::UpdateNetSupplierInfo(uint32_t supplierId, const sptr<NetSupplierInfo> &netSupplierInfo)
{
    return 0;
}
int32_t NetConnClient::UpdateNetLinkInfo(uint32_t supplierId, const sptr<NetLinkInfo> &netLinkInfo)
{
    return 0;
}
int32_t NetConnClient::RegisterNetConnCallback(const sptr<INetConnCallback> &callback)
{
    return 0;
}
int32_t NetConnClient::RegisterNetConnCallback(const sptr<NetSpecifier> &netSpecifier,
    const sptr<INetConnCallback> &callback, const uint32_t &timeoutMS)
{
    return 0;
}
int32_t NetConnClient::UnregisterNetConnCallback(const sptr<INetConnCallback> &callback)
{
    return 0;
}
int32_t NetConnClient::GetDefaultNet(NetHandle &netHandle)
{
    return 0;
}
int32_t NetConnClient::HasDefaultNet(bool &flag)
{
    return 0;
}
int32_t NetConnClient::GetAllNets(std::list<sptr<NetHandle>> &netList)
{
    return 0;
}
int32_t NetConnClient::GetConnectionProperties(const NetHandle &netHandle, NetLinkInfo &info)
{
    return 0;
}
int32_t NetConnClient::GetNetCapabilities(const NetHandle &netHandle, NetAllCapabilities &netAllCap)
{
    return 0;
}
int32_t NetConnClient::GetAddressesByName(const std::string &host, int32_t netId, std::vector<INetAddr> &addrList)
{
    return 0;
}
int32_t NetConnClient::GetAddressByName(const std::string &host, int32_t netId, INetAddr &addr)
{
    return 0;
}
int32_t NetConnClient::BindSocket(int32_t socket_fd, int32_t netId)
{
    return 0;
}
int32_t NetConnClient::NetDetection(const NetHandle &netHandle)
{
    return 0;
}
int32_t NetConnClient::SetAirplaneMode(bool state)
{
    return 0;
}
int32_t NetConnClient::IsDefaultNetMetered(bool &isMetered)
{
    return 0;
}
int32_t NetConnClient::SetGlobalHttpProxy(const HttpProxy &httpProxy)
{
    return 0;
}
int32_t NetConnClient::GetGlobalHttpProxy(HttpProxy &httpProxy)
{
    return 0;
}
int32_t NetConnClient::GetDefaultHttpProxy(HttpProxy &httpProxy)
{
    return 0;
}
int32_t NetConnClient::SetAppNet(int32_t netId)
{
    return 0;
}
int32_t NetConnClient::GetAppNet(int32_t &netId)
{
    return 0;
}
int32_t NetConnClient::GetNetIdByIdentifier(const std::string &ident, std::list<int32_t> &netIdList)
{
    return 0;
}
int32_t NetConnClient::RegisterNetInterfaceCallback(const sptr<INetInterfaceStateCallback> &callback)
{
    return 0;
}
int32_t NetConnClient::GetNetInterfaceConfiguration(const std::string &iface, NetInterfaceConfiguration &config)
{
    return 0;
}
NetConnClient::NetConnClient() {}
NetConnClient::~NetConnClient() {}
sptr<INetConnService> NetConnClient::GetProxy()
{
    return sptr<INetConnService>();
}
void NetConnClient::OnRemoteDied(const wptr<IRemoteObject> &remote) {}

NetConnCallbackStub::~NetConnCallbackStub() {}
NetConnCallbackStub::NetConnCallbackStub() {}
int32_t NetConnCallbackStub::OnRemoteRequest(uint32_t code, MessageParcel &data, MessageParcel &reply,
    MessageOption &option)
{
    return IPCObjectStub::OnRemoteRequest(code, data, reply, option);
}
int32_t NetConnCallbackStub::NetAvailable(sptr<NetHandle> &netHandle)
{
    return 0;
}
int32_t NetConnCallbackStub::NetCapabilitiesChange(sptr<NetHandle> &netHandle,
    const sptr<NetAllCapabilities> &netAllCap)
{
    return 0;
}
int32_t NetConnCallbackStub::NetConnectionPropertiesChange(sptr<NetHandle> &netHandle, const sptr<NetLinkInfo> &info)
{
    return 0;
}
int32_t NetConnCallbackStub::NetLost(sptr<NetHandle> &netHandle)
{
    return 0;
}
int32_t NetConnCallbackStub::NetUnavailable()
{
    return 0;
}
int32_t NetConnCallbackStub::NetBlockStatusChange(sptr<NetHandle> &netHandle, bool blocked)
{
    return 0;
}
int32_t NetConnCallbackStub::OnNetAvailable(MessageParcel &data, MessageParcel &reply)
{
    return 0;
}
int32_t NetConnCallbackStub::OnNetCapabilitiesChange(MessageParcel &data, MessageParcel &reply)
{
    return 0;
}
int32_t NetConnCallbackStub::OnNetConnectionPropertiesChange(MessageParcel &data, MessageParcel &reply)
{
    return 0;
}
int32_t NetConnCallbackStub::OnNetLost(MessageParcel &data, MessageParcel &reply)
{
    return 0;
}
int32_t NetConnCallbackStub::OnNetUnavailable(MessageParcel &data, MessageParcel &reply)
{
    return 0;
}
int32_t NetConnCallbackStub::OnNetBlockStatusChange(MessageParcel &data, MessageParcel &reply)
{
    return 0;
}
}
}