#include "distributed_file_daemon_manager.h"
#include "distributed_file_daemon_manager_impl.h"
#include "remote_file_share.h"
#include "asset/asset_recv_callback_stub.h"
#include "asset/asset_send_callback_stub.h"
#include "asset/asset_obj.h"

namespace OHOS {
namespace AppFileService {
namespace ModuleRemoteFileShare {
int RemoteFileShare::CreateSharePath(const int &fd, std::string &sharePath, const int &userId,
    const std::string &deviceId)
{
    return 0;
}
int32_t RemoteFileShare::GetDfsUriFromLocal(const std::string &uriStr, const int32_t &userId, HmdfsUriInfo &hui)
{
    return 0;
}
int32_t RemoteFileShare::GetDfsUrisFromLocal(const std::vector<std::string> &uriList, const int32_t &userId,
    std::unordered_map<std::string, HmdfsUriInfo> &uriToDfsUriMaps)
{
    return 0;
}
} // namespace ModuleRemoteFileShare
} // namespace AppFileService
namespace Storage {
namespace DistributedFile {

int32_t AssetRecvCallbackStub::OnRemoteRequest(uint32_t code, MessageParcel &data, MessageParcel &reply, MessageOption &option)
{
    return 0;
}
AssetRecvCallbackStub::AssetRecvCallbackStub() {}
int32_t AssetSendCallbackStub::OnRemoteRequest(uint32_t code, MessageParcel &data, MessageParcel &reply, MessageOption &option)
{
    return 0;
}
AssetSendCallbackStub::AssetSendCallbackStub() {}
int32_t DistributedFileDaemonManagerImpl::CloseP2PConnection(const DistributedHardware::DmDeviceInfo &deviceInfo)
{
    return 0;
}
DistributedFileDaemonManagerImpl &DistributedFileDaemonManagerImpl::GetInstance()
{
    static DistributedFileDaemonManagerImpl manager;
    return manager;
}
int32_t DistributedFileDaemonManagerImpl::OpenP2PConnection(const DistributedHardware::DmDeviceInfo &deviceInfo)
{
    return 0;
}
int32_t DistributedFileDaemonManagerImpl::RequestSendFile(const std::string &srcUri, const std::string &dstPath,
    const std::string &remoteDeviceId, const std::string &sessionName)
{
    return 0;
}
int32_t DistributedFileDaemonManagerImpl::OpenP2PConnectionEx(const std::string &networkId,
    sptr<IFileDfsListener> remoteReverseObj)
{
    return 0;
}
int32_t DistributedFileDaemonManagerImpl::CloseP2PConnectionEx(const std::string &networkId)
{
    return 0;
}
int32_t DistributedFileDaemonManagerImpl::PrepareSession(const std::string &srcUri, const std::string &dstUri,
    const std::string &srcDeviceId, const sptr<IRemoteObject> &listener, HmdfsInfo &info)
{
    return 0;
}
int32_t DistributedFileDaemonManagerImpl::CancelCopyTask(const std::string &sessionName)
{
    return 0;
}
int32_t DistributedFileDaemonManagerImpl::GetRemoteCopyInfo(const std::string &srcUri, bool &isFile, bool &isDir)
{
    return 0;
}
int32_t DistributedFileDaemonManagerImpl::PushAsset(int32_t userId, const sptr<AssetObj> &assetObj,
    const sptr<IAssetSendCallback> &sendCallback)
{
    return 0;
}
int32_t DistributedFileDaemonManagerImpl::RegisterAssetCallback(const sptr<IAssetRecvCallback> &recvCallback)
{
    return 0;
}
int32_t DistributedFileDaemonManagerImpl::UnRegisterAssetCallback(const sptr<IAssetRecvCallback> &recvCallback)
{
    return 0;
}

DistributedFileDaemonManager &DistributedFileDaemonManager::GetInstance()
{
    return DistributedFileDaemonManagerImpl::GetInstance();
}
bool AssetObj::Marshalling(Parcel &parcel) const
{
    return true;
}
bool AssetObj::ReadFromParcel(Parcel &parcel)
{
    return true;
}
AssetObj *AssetObj::Unmarshalling(Parcel &parcel)
{
    return nullptr;
}
} // namespace DistributedFile
} // namespace Storage
} // namespace OHOS