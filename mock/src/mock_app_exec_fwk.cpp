/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "bundle_mgr_helper.h"
#include "extension.h"
#include "element_name.h"
#include "napi_base_context.h"
#include "ability.h"
#include "ability_local_record.h"
namespace OHOS {
namespace AbilityRuntime {
AppExecFwk::Ability *GetCurrentAbility(napi_env env) { return new AppExecFwk::Ability(); }
}
namespace AppExecFwk {

bool BundleMgrHelper::GetBundleInfo(const std::string &bundleName, const BundleFlag flag, BundleInfo &bundleInfo, int32_t userId){
    return true;
}
ErrCode BundleMgrHelper::GetNameForUid(const int32_t uid, string& name)
{
    return 0;
}
ErrCode BundleMgrHelper::InstallSandboxApp(const string& bundleName, int32_t dlpType, int32_t userId, int32_t& appIndex)
{
    return 0;
}
ErrCode BundleMgrHelper::UninstallSandboxApp(const string& bundleName, int32_t appIndex, int32_t userId)
{
    return 0;
}
ErrCode BundleMgrHelper::GetUninstalledBundleInfo(const std::string bundleName, BundleInfo& bundleInfo)
{
    return 0;
}
ErrCode BundleMgrHelper::GetSandboxBundleInfo(const string& bundleName, int32_t appIndex, int32_t userId,
    BundleInfo& info)
{
    return 0;
}
ErrCode BundleMgrHelper::GetSandboxAbilityInfo(const Want& want, int32_t appIndex, int32_t flags, int32_t userId,
    AbilityInfo& abilityInfo)
{
    return 0;
}
ErrCode BundleMgrHelper::GetSandboxExtAbilityInfos(const Want& want, int32_t appIndex, int32_t flags, int32_t userId,
    std::vector<ExtensionAbilityInfo>& extensionInfos)
{
    return 0;
}
ErrCode BundleMgrHelper::GetSandboxHapModuleInfo(const AbilityInfo& abilityInfo, int32_t appIndex, int32_t userId,
    HapModuleInfo& hapModuleInfo)
{
    return 0;
}
bool BundleMgrHelper::GetBundleInfo(const string& bundleName, int32_t flags, BundleInfo& bundleInfo, int32_t userId)
{
    return false;
}
bool BundleMgrHelper::GetHapModuleInfo(const AbilityInfo& abilityInfo, HapModuleInfo& hapModuleInfo)
{
    return false;
}
std::string BundleMgrHelper::GetAbilityLabel(const string& bundleName, const string& abilityName)
{
    return std::string();
}
std::string BundleMgrHelper::GetAppType(const string& bundleName)
{
    return std::string();
}
bool BundleMgrHelper::QueryAbilityInfo(const Want& want, AbilityInfo& abilityInfo)
{
    return false;
}
bool BundleMgrHelper::QueryAbilityInfo(const Want& want, int32_t flags, int32_t userId, AbilityInfo& abilityInfo)
{
    return false;
}
bool BundleMgrHelper::GetBundleInfos(int32_t flags, std::vector<BundleInfo>& bundleInfos, int32_t userId)
{
    return false;
}
bool BundleMgrHelper::ProcessPreload(const Want& want)
{
    return false;
}
bool BundleMgrHelper::QueryExtensionAbilityInfos(const Want& want, const int32_t& flag, const int32_t& userId,
    std::vector<ExtensionAbilityInfo>& extensionInfos)
{
    return false;
}
ErrCode BundleMgrHelper::GetBundleInfoV9(const string& bundleName, int32_t flags, BundleInfo& bundleInfo,
    int32_t userId)
{
    return 0;
}
bool BundleMgrHelper::GetApplicationInfo(const string& appName, const ApplicationFlag flag, const int32_t userId,
    ApplicationInfo& appInfo)
{
    return false;
}
bool BundleMgrHelper::GetApplicationInfo(const string& appName, int32_t flags, int32_t userId, ApplicationInfo& appInfo)
{
    return false;
}
ErrCode BundleMgrHelper::GetJsonProfile(ProfileType profileType, const string& bundleName, const string& moduleName,
    string& profile, int32_t userId)
{
    return 0;
}
bool BundleMgrHelper::UnregisterBundleEventCallback(const sptr<IBundleEventCallback>& bundleEventCallback)
{
    return false;
}
bool BundleMgrHelper::QueryExtensionAbilityInfoByUri(const string& uri, int32_t userId,
    ExtensionAbilityInfo& extensionAbilityInfo)
{
    return false;
}
bool BundleMgrHelper::ImplicitQueryInfoByPriority(const Want& want, int32_t flags, int32_t userId,
    AbilityInfo& abilityInfo, ExtensionAbilityInfo& extensionInfo)
{
    return false;
}
bool BundleMgrHelper::QueryAbilityInfoByUri(const string& abilityUri, int32_t userId, AbilityInfo& abilityInfo)
{
    return false;
}
bool BundleMgrHelper::QueryAbilityInfo(const Want& want, int32_t flags, int32_t userId, AbilityInfo& abilityInfo,
    const sptr<IRemoteObject>& callBack)
{
    return false;
}
void BundleMgrHelper::UpgradeAtomicService(const Want& want, int32_t userId) {}
bool BundleMgrHelper::ImplicitQueryInfos(const Want& want, int32_t flags, int32_t userId, bool withDefault,
    std::vector<AbilityInfo>& abilityInfos, std::vector<ExtensionAbilityInfo>& extensionInfos)
{
    return false;
}
bool BundleMgrHelper::CleanBundleDataFiles(const string& bundleName, const int32_t userId)
{
    return false;
}
bool BundleMgrHelper::GetBundleGidsByUid(const string& bundleName, const int32_t& uid, std::vector<int32_t>& gids)
{
    return false;
}
bool BundleMgrHelper::RegisterBundleEventCallback(const sptr<IBundleEventCallback>& bundleEventCallback)
{
    return false;
}
bool BundleMgrHelper::GetBundleInfos(const BundleFlag flag, std::vector<BundleInfo>& bundleInfos, int32_t userId)
{
    return false;
}
bool BundleMgrHelper::GetHapModuleInfo(const AbilityInfo& abilityInfo, int32_t userId, HapModuleInfo& hapModuleInfo)
{
    return false;
}
bool BundleMgrHelper::QueryAppGalleryBundleName(string& bundleName)
{
    return false;
}
ErrCode BundleMgrHelper::GetUidByBundleName(const string& bundleName, const int32_t userId)
{
    return 0;
}
ErrCode BundleMgrHelper::QueryExtensionAbilityInfosOnlyWithTypeName(const string& extensionTypeName,
    const uint32_t flag, const int32_t userId, std::vector<ExtensionAbilityInfo>& extensionInfos)
{
    return 0;
}
sptr<IDefaultApp> BundleMgrHelper::GetDefaultAppProxy()
{
    return sptr<IDefaultApp>();
}
ErrCode BundleMgrHelper::GetLaunchWantForBundle(const string& bundleName, Want& want, int32_t userId)
{
    return 0;
}
sptr<IBundleMgr> BundleMgrHelper::Connect()
{
    return sptr<IBundleMgr>();
}
sptr<IBundleInstaller> BundleMgrHelper::ConnectBundleInstaller()
{
    return sptr<IBundleInstaller>();
}
void BundleMgrHelper::OnDeath() {}
std::string BundleMgrHelper::ParseBundleNameByAppId(const string& appId) const
{
    return std::string();
}
BundleMgrHelper::BundleMgrHelper() {}
BundleMgrHelper::~BundleMgrHelper() {}

ElementName::ElementName(const string &deviceId, const string &bundleName, const string &abilityName,
                         const string &moduleName){}
ElementName::ElementName() {}
ElementName::~ElementName() {}
bool ElementName::Marshalling(Parcel &parcel) const { return false; }
ElementName *ElementName::Unmarshalling(Parcel &parcel) { return nullptr;}
std::string ElementName::GetURI() const { return ""; }
int Ability::TerminateAbility(Want &want) { return 0; }
Ability *Ability::Create(const std::unique_ptr<AbilityRuntime::Runtime> &runtime) { return nullptr; }
void Ability::AddActionRoute(const string &action, const string &entry) { }
ErrCode Ability::TerminateAbility() { return SUBSYS_COMMON; }
std::shared_ptr<LifeCycle> Ability::GetLifecycle() { return std::shared_ptr<LifeCycle>(); }
std::shared_ptr<Global::Resource::ResourceManager> Ability::GetResourceManager() const { return nullptr; }
bool Ability::IsUpdatingConfigurations() { return false; }
ErrCode Ability::StartAbilityForResult(const Want &want, int requestCode) { return 0; }
ErrCode Ability::StartAbilityForResult(const Want &want, int requestCode, AbilityStartSetting abilityStartSetting)
{
    return 0;
}
ErrCode Ability::StartAbility(const Want &want, AbilityStartSetting abilityStartSetting) { return 0; }
ErrCode Ability::StartFeatureAbilityForResult(const Want &want, int requestCode, FeatureAbilityTask &&task){ return 0;}
void Ability::Init(const std::shared_ptr<AbilityInfo> &abilityInfo, const std::shared_ptr<OHOSApplication> &application,
                   std::shared_ptr<AbilityHandler> &handler, const sptr<IRemoteObject> &token){}
void Ability::AttachAbilityContext(const std::shared_ptr<AbilityRuntime::AbilityContext> &abilityContext) {}
void Ability::OnStart(const Want &want) {}
void Ability::OnStop() {}
void Ability::OnActive(){}
void Ability::OnInactive(){}
sptr<IRemoteObject> Ability::OnConnect(const Want &want){ return sptr<IRemoteObject>();}
void Ability::OnDisconnect(const Want &want){}
void Ability::OnRequestPermissionsFromUserResult(int requestCode, const std::vector<std::string> &permissions,
                                                 const std::vector<int> &grantResults){}
std::string Ability::GetType(const Uri &uri){  return std::string();}
int Ability::Insert(const Uri &uri, const NativeRdb::ValuesBucket &value){ return 0;}
std::shared_ptr<AppExecFwk::PacMap> Ability::Call(const Uri &uri, const string &method, const string &arg,
                                                  const PacMap &pacMap)
{
    return std::shared_ptr<AppExecFwk::PacMap>();
}
void Ability::OnConfigurationUpdated(const Configuration &configuration){}
void Ability::OnConfigurationUpdatedNotify(const Configuration &configuration){}
void Ability::OnMemoryLevel(int level){}
int Ability::OpenRawFile(const Uri &uri, const string &mode){  return 0;}
int Ability::Update(const Uri &uri, const NativeRdb::ValuesBucket &value,
                    const NativeRdb::DataAbilityPredicates &predicates){ return 0;}
std::shared_ptr<OHOSApplication> Ability::GetApplication(){ return std::shared_ptr<OHOSApplication>();}
std::string Ability::GetAbilityName(){ return std::string();}
bool Ability::IsTerminating(){ return false;}
void Ability::OnAbilityResult(int requestCode, int resultCode, const Want &resultData){}
void Ability::OnFeatureAbilityResult(int requestCode, int resultCode, const Want &resultData){}
void Ability::OnBackPressed(){}
void Ability::OnNewWant(const Want &want){}
void Ability::OnRestoreAbilityState(const PacMap &inState){}
void Ability::OnSaveAbilityState(PacMap &outState){}
void Ability::OnEventDispatch(){}
void Ability::SetWant(const Want &want){}
std::shared_ptr<AAFwk::Want> Ability::GetWant() { return std::shared_ptr<AAFwk::Want>();}
void Ability::SetResult(int resultCode, const Want &resultData){}
void Ability::OnCommand(const Want &want, bool restart, int startId){}
void Ability::Dump(const string &extra){}
void Ability::Dump(const std::vector<std::string> &params, std::vector<std::string> &info){}
void Ability::KeepBackgroundRunning(int id, const NotificationRequest &notificationRequest){}
void Ability::CancelBackgroundRunning() {}
Uri Ability::NormalizeUri(const Uri &uri) { return Uri(""); }
int Ability::Delete(const Uri &uri, const NativeRdb::DataAbilityPredicates &predicates){ return 0;}
std::vector<std::string> Ability::GetFileTypes(const Uri &uri, const string &mimeTypeFilter){ return {};}
int Ability::OpenFile(const Uri &uri, const string &mode) { return 0; }
std::shared_ptr<NativeRdb::AbsSharedResultSet> Ability::Query(const Uri &uri, const std::vector<std::string> &columns,
                                                              const NativeRdb::DataAbilityPredicates &predicates)
{
    return std::shared_ptr<NativeRdb::AbsSharedResultSet>();
}

void Ability::SetMainRoute(const string &entry) { }
void Ability::ContinueAbilityReversibly(const string &deviceId){}
std::string Ability::GetOriginalDeviceId() { return "networkId";}
ContinuationState Ability::GetContinuationState() { return ContinuationState::REPLICA_RUNNING;}
std::shared_ptr<AbilityPackage> Ability::GetAbilityPackage() { return nullptr; }
Uri Ability::DenormalizeUri(const Uri &uri) { return Uri(""); }
bool Ability::Reload(const Uri &uri, const PacMap &extras) { return false; }
int Ability::BatchInsert(const Uri &uri, const std::vector<NativeRdb::ValuesBucket> &values) { return 0; }
AbilityLifecycleExecutor::LifecycleState Ability::GetState() { return AbilityLifecycleExecutor::BACKGROUND; }
ErrCode Ability::StartAbility(const Want &want) { return 0; }
bool Ability::ConnectAbility(const Want &want, const sptr<AAFwk::IAbilityConnection> &conn) { return false;}
ErrCode Ability::DisconnectAbility(const sptr<AAFwk::IAbilityConnection> &conn) { return SUBSYS_COMMON;}
bool Ability::StopAbility(const Want &want) { return false;}
void Ability::DestroyInstance() {}
void Ability::PostTask(std::function<void()> task, long delayTime) {}
Uri Ability::OnSetCaller() { return Uri("");}
std::shared_ptr<AbilityPostEventTimeout> Ability::CreatePostEventTimeouter(std::string taskstr) { return nullptr;}
int Ability::StartBackgroundRunning(const AbilityRuntime::WantAgent::WantAgent &wantAgent) { return 0; }
int Ability::StopBackgroundRunning() { return 0;}
std::string Ability::GetErrorMsg(const ErrCode errorCode) { return "";}
sptr<IBundleMgr> Ability::GetBundleMgr() { return sptr<IBundleMgr>();}
void Ability::SetBundleManager(const sptr<IBundleMgr> &bundleManager) { }
std::weak_ptr<IContinuationRegisterManager> Ability::GetContinuationRegisterManager()
{
    return std::weak_ptr<IContinuationRegisterManager>();
}
int32_t Ability::OnContinue(WantParams &wantParams) { return 0;}
void Ability::ContinueAbilityWithStack(const string &deviceId, uint32_t versionCode) {}
void Ability::ContinueAbility(const string &deviceId) {}
bool Ability::OnStartContinuation() { return false;}
std::vector<std::shared_ptr<DataAbilityResult>> Ability::ExecuteBatch(
    const std::vector<std::shared_ptr<DataAbilityOperation>> &operations) { return {};}
void Ability::ExecuteOperation(std::shared_ptr<DataAbilityOperation> &operation,
                               std::vector<std::shared_ptr<DataAbilityResult>> &results, int index) {}
bool Ability::OnSaveData(WantParams &saveData) { return false;}
bool Ability::OnRestoreData(WantParams &restoreData) { return false;}
void Ability::OnCompleteContinuation(int result) {}
void Ability::OnRemoteTerminated() {}
sptr<IRemoteObject> Ability::CallRequest() { return sptr<IRemoteObject>();}
const AAFwk::LaunchParam &Ability::GetLaunchParam() const
{
    static LaunchParam launchParam;
    return launchParam;
}
bool Ability::IsRestoredInContinuation() const { return false;}
void Ability::WaitingDistributedObjectSyncComplete(const Want &want) {}
void Ability::NotityContinuationResult(const Want &want, bool success) {}
std::shared_ptr<NativeRdb::DataAbilityPredicates> Ability::ParsePredictionArgsReference(
    std::vector<std::shared_ptr<DataAbilityResult>> &results, std::shared_ptr<DataAbilityOperation> &operation,
    int numRefs)
{
    return std::shared_ptr<NativeRdb::DataAbilityPredicates>();
}

std::shared_ptr<NativeRdb::ValuesBucket> Ability::ParseValuesBucketReference(
    std::vector<std::shared_ptr<DataAbilityResult>> &results, std::shared_ptr<DataAbilityOperation> &operation,
    int numRefs)
{
    return std::shared_ptr<NativeRdb::ValuesBucket>();
}
int Ability::ChangeRef2Value(std::vector<std::shared_ptr<DataAbilityResult>> &results, int numRefs, int index){ return 0;}
bool Ability::CheckAssertQueryResult(std::shared_ptr<NativeRdb::AbsSharedResultSet> &queryResult,
                                     std::shared_ptr<NativeRdb::ValuesBucket> &&valuesBucket){ return false;}
void Ability::DispatchLifecycleOnForeground(const Want &want) {}
bool Ability::VerifySupportForContinuation() { return false;}
void Ability::HandleCreateAsContinuation(const Want &want) {}
bool Ability::IsFlagExists(unsigned int flag, unsigned int flagSet) {return false;}
void Ability::SetStartAbilitySetting(std::shared_ptr<AbilityStartSetting> setting){}
void Ability::SetLaunchParam(const LaunchParam &launchParam) {}
const std::shared_ptr<AbilityInfo> &AbilityLocalRecord::GetAbilityInfo() { return abilityInfo_; }
bool AbilityInfo::ReadFromParcel(Parcel &parcel) { return false; }
bool AbilityInfo::Marshalling(Parcel &parcel) const { return false; }
AbilityInfo *AbilityInfo::Unmarshalling(Parcel &parcel) { return nullptr; }
void AbilityInfo::Dump(std::string prefix, int fd) {}
void AbilityInfo::ConvertToCompatiableAbilityInfo(CompatibleAbilityInfo& compatibleAbilityInfo) const {}
}
}