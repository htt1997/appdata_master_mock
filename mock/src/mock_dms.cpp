/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "dms_handler.h"
#include "dms_listener_stub.h"

namespace OHOS {
namespace DistributedSchedule {
namespace {
const std::string TAG = "DmsHandle";
}
int32_t DSchedEventListenerStub::OnRemoteRequest(uint32_t code, MessageParcel &data, MessageParcel &reply,
    MessageOption &option)
{
}
DSchedEventListenerStub::DSchedEventListenerStub() {}
DSchedEventListenerStub::~DSchedEventListenerStub() {}
void DSchedEventListenerStub::DSchedEventNotifyInner(MessageParcel &data, MessageParcel &reply) {}

DmsHandler &DmsHandler::GetInstance()
{
    static DmsHandler instance;
    return instance;
}

int32_t DmsHandler::GetContinueInfo(ContinueInfo &continueInfo)
{
    return 0;
}

int32_t DmsHandler::GetDSchedEventInfo(const DSchedEventType &type, std::vector<EventNotify> &events)
{
    return 0;
}

int32_t DmsHandler::RegisterDSchedEventListener(const DSchedEventType &type, sptr<IDSchedEventListener> &listener)
{
    return 0;
}

int32_t DmsHandler::UnRegisterDSchedEventListener(const DSchedEventType &type, sptr<IDSchedEventListener> &listener)
{
    return 0;
}
} // namespace DistributedSchedule
} // namespace OHOS