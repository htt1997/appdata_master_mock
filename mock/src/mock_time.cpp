/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "time_service_client.h"

namespace OHOS {
namespace MiscServices {
sptr<TimeServiceClient> TimeServiceClient::GetInstance(){
    static sptr<TimeServiceClient> instance;
    return instance;
}

TimeServiceClient::~TimeServiceClient() {}
void TimeServiceClient::RefPtrCallback()
{
    RefBase::RefPtrCallback();
}
void TimeServiceClient::OnFirstStrongRef(const void* objectId)
{
    RefBase::OnFirstStrongRef(objectId);
}
void TimeServiceClient::OnLastStrongRef(const void* objectId)
{
    RefBase::OnLastStrongRef(objectId);
}
void TimeServiceClient::OnLastWeakRef(const void* objectId)
{
    RefBase::OnLastWeakRef(objectId);
}
bool TimeServiceClient::OnAttemptPromoted(const void* objectId)
{
    return RefBase::OnAttemptPromoted(objectId);
}
bool TimeServiceClient::SetTime(int64_t milliseconds)
{
    return false;
}
bool TimeServiceClient::SetTime(int64_t milliseconds, int32_t& code)
{
    return false;
}
int32_t TimeServiceClient::SetTimeV9(int64_t time)
{
    return 0;
}
bool TimeServiceClient::SetTimeZone(const std::string& timeZoneId)
{
    return false;
}
bool TimeServiceClient::SetTimeZone(const std::string& timezoneId, int32_t& code)
{
    return false;
}
int32_t TimeServiceClient::SetTimeZoneV9(const std::string& timezoneId)
{
    return 0;
}
std::string TimeServiceClient::GetTimeZone()
{
    return std::string();
}
int32_t TimeServiceClient::GetTimeZone(std::string& timezoneId)
{
    return 0;
}
int64_t TimeServiceClient::GetWallTimeMs()
{
    return 0;
}
int32_t TimeServiceClient::GetWallTimeMs(int64_t& time)
{
    return 0;
}
int64_t TimeServiceClient::GetWallTimeNs()
{
    return 0;
}
int32_t TimeServiceClient::GetWallTimeNs(int64_t& time)
{
    return 0;
}
int64_t TimeServiceClient::GetBootTimeMs()
{
    return 0;
}
int32_t TimeServiceClient::GetBootTimeMs(int64_t& time)
{
    return 0;
}
int64_t TimeServiceClient::GetBootTimeNs()
{
    return 0;
}
int32_t TimeServiceClient::GetBootTimeNs(int64_t& time)
{
    return 0;
}
int64_t TimeServiceClient::GetMonotonicTimeMs()
{
    return 0;
}
int32_t TimeServiceClient::GetMonotonicTimeMs(int64_t& time)
{
    return 0;
}
int64_t TimeServiceClient::GetMonotonicTimeNs()
{
    return 0;
}
int32_t TimeServiceClient::GetMonotonicTimeNs(int64_t& time)
{
    return 0;
}
int64_t TimeServiceClient::GetThreadTimeMs()
{
    return 0;
}
int32_t TimeServiceClient::GetThreadTimeMs(int64_t& time)
{
    return 0;
}
int64_t TimeServiceClient::GetThreadTimeNs()
{
    return 0;
}
int32_t TimeServiceClient::GetThreadTimeNs(int64_t& time)
{
    return 0;
}
uint64_t TimeServiceClient::CreateTimer(std::shared_ptr<ITimerInfo> timerInfo)
{
    return 0;
}
int32_t TimeServiceClient::CreateTimerV9(std::shared_ptr<ITimerInfo> timerOptions, uint64_t& timerId)
{
    return 0;
}
bool TimeServiceClient::StartTimer(uint64_t timerId, uint64_t triggerTime)
{
    return false;
}
int32_t TimeServiceClient::StartTimerV9(uint64_t timerId, uint64_t triggerTime)
{
    return 0;
}
bool TimeServiceClient::StopTimer(uint64_t timerId)
{
    return false;
}
int32_t TimeServiceClient::StopTimerV9(uint64_t timerId)
{
    return 0;
}
bool TimeServiceClient::DestroyTimer(uint64_t timerId)
{
    return false;
}
int32_t TimeServiceClient::DestroyTimerV9(uint64_t timerId)
{
    return 0;
}
bool TimeServiceClient::ProxyTimer(int32_t uid, bool isProxy, bool needRetrigger)
{
    return false;
}
bool TimeServiceClient::ProxyTimer(std::set<int> pidList, bool isProxy, bool needRetrigger)
{
    return false;
}
int32_t TimeServiceClient::AdjustTimer(bool isAdjust, uint32_t interval)
{
    return 0;
}
int32_t TimeServiceClient::SetTimerExemption(const std::unordered_set<std::string> nameArr, bool isExemption)
{
    return 0;
}
bool TimeServiceClient::ResetAllProxy()
{
    return false;
}
}
} // namespace OHOS