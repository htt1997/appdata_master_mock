/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define LOG_TAG "mock_mem_mgr"
#include "bundle_priority_list.h"
#include "mem_mgr_client.h"
#include "mem_mgr_process_state_info.h"
#include "mem_mgr_proxy.h"
#include "mem_mgr_window_info.h"

namespace OHOS::Memory {
int32_t BundlePriorityList::GetCount() const
{
    return 0;
}
bool BundlePriorityList::Marshalling(Parcel &parcel) const
{
    return false;
}
BundlePriorityList *BundlePriorityList::Unmarshalling(Parcel &parcel)
{
    return nullptr;
}
void BundlePriorityList::SetCount(int32_t count) {}
const std::vector<BundlePriority> &BundlePriorityList::GetList()
{
    static std::vector<BundlePriority> LIST = {};
    return LIST;
}
int32_t BundlePriorityList::Size() const
{
    return 0;
}
void BundlePriorityList::AddBundleInfo(BundlePriority &bundleInfo) {}
void BundlePriorityList::Show() const {}
bool BundlePriorityList::ReadFromParcel(Parcel &parcel)
{
    return false;
}
int32_t MemMgrClient::GetBundlePriorityList(BundlePriorityList &bundlePrioList)
{
    return 0;
}
int32_t MemMgrClient::NotifyDistDevStatus(int32_t pid, int32_t uid, const std::string &name, bool connected)
{
    return 0;
}
bool MemMgrProcessStateInfo::Marshalling(Parcel &parcel) const
{
    return 0;
}
MemMgrProcessStateInfo *MemMgrProcessStateInfo::Unmarshalling(Parcel &parcel)
{
    return nullptr;
}
MemMgrClient &MemMgrClient::GetInstance(){
    static MemMgrClient memMgrClient;
    return memMgrClient;
}
int32_t MemMgrClient::RegisterActiveApps(int32_t pid, int32_t uid)
{
    return 0;
}
int32_t MemMgrClient::GetKillLevelOfLmkd(int32_t &killLevel)
{
    return 0;
}
int32_t MemMgrClient::DeregisterActiveApps(int32_t pid, int32_t uid)
{
    return 0;
}
int32_t MemMgrClient::GetAvailableMemory(int32_t &memSize)
{
    return 0;
}
int32_t MemMgrClient::GetTotalMemory(int32_t &memSize)
{
    return 0;
}
int32_t MemMgrClient::GetAvailableMemory()
{
    return 0;
}
int32_t MemMgrClient::GetTotalMemory()
{
    return 0;
}
int32_t MemMgrClient::OnWindowVisibilityChanged(const std::vector<sptr<MemMgrWindowInfo>> &MemMgrWindowInfo)
{
    return 0;
}
int32_t MemMgrClient::GetReclaimPriorityByPid(int32_t pid, int32_t &priority)
{
    return 0;
}
int32_t MemMgrClient::NotifyProcessStateChangedSync(const MemMgrProcessStateInfo &processStateInfo)
{
    return 0;
}
int32_t MemMgrClient::NotifyProcessStateChangedAsync(const MemMgrProcessStateInfo &processStateInfo)
{
    return 0;
}
int32_t MemMgrClient::NotifyProcessStatus(int32_t pid, int32_t type, int32_t status, int saId)
{
    return 0;
}
int32_t MemMgrClient::SetCritical(int32_t pid, bool critical, int32_t saId)
{
    return 0;
}
sptr<IMemMgr> MemMgrClient::GetMemMgrService()
{
    return sptr<IMemMgr>();
}
int32_t MemMgrProxy::GetBundlePriorityList(BundlePriorityList &bundlePrioList)
{
    return 0;
}
int32_t MemMgrProxy::NotifyDistDevStatus(int32_t pid, int32_t uid, const std::string &name, bool connected)
{
    return 0;
}
int32_t MemMgrProxy::GetKillLevelOfLmkd(int32_t &killLevel)
{
    return 0;
}
int32_t MemMgrProxy::GetReclaimPriorityByPid(int32_t pid, int32_t &priority)
{
    return 0;
}
int32_t MemMgrProxy::NotifyProcessStateChangedSync(const MemMgrProcessStateInfo &processStateInfo)
{
    return 0;
}
int32_t MemMgrProxy::NotifyProcessStateChangedAsync(const MemMgrProcessStateInfo &processStateInfo)
{
    return 0;
}
int32_t MemMgrProxy::NotifyProcessStatus(int32_t pid, int32_t type, int32_t status, int saId)
{
    return 0;
}
int32_t MemMgrProxy::SetCritical(int32_t pid, bool critical, int32_t saId)
{
    return 0;
}
} // namespace OHOS::Memory