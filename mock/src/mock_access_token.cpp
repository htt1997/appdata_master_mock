/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "accesstoken_kit.h"
#include "nativetoken_kit.h"
#include "token_setproc.h"
static constexpr const char *BundleNames[6] = { "invalid", "com.huawei.ohos.toteweather",
    "com.ohos.kvdatamanager.test", "ohos.test.demo", "ohos.test.demo1", "ohos.test.demo2" };
static constexpr const char *AppId[4] = { "invalid", "com.huawei.toteweather_adfasdflaskdfasdf", "",
    "ohos.test.demo_ABSEED" };
static constexpr const char *NativeName[4] = { "invalid", "foundation", "distributed_test", "msdp_sa" };
namespace OHOS::Security::AccessToken {
ATokenTypeEnum AccessTokenKit::GetTokenTypeFlag(AccessTokenID tokenID)
{
    AccessTokenIDInner *inner = (AccessTokenIDInner *)&tokenID;
    return ATokenTypeEnum(inner->type);
}

int AccessTokenKit::VerifyAccessToken(AccessTokenID tokenID, const std::string &permissionName)
{
    return PERMISSION_GRANTED;
}

int AccessTokenKit::GetNativeTokenInfo(AccessTokenID tokenID, NativeTokenInfo &nativeTokenInfoRes)
{
    AccessTokenIDInner *inner = (AccessTokenIDInner *)&tokenID;
    nativeTokenInfoRes.tokenID = tokenID;
    nativeTokenInfoRes.processName = "msdp_sa";
    return 0;
}

AccessTokenID AccessTokenKit::GetNativeTokenId(const std::string &processName)
{
    uint32_t tokenId = 0;
    auto *inner = (AccessTokenIDInner *)&tokenId;
    return tokenId;
}

int AccessTokenKit::GetHapTokenInfo(AccessTokenID tokenID, HapTokenInfo &hapTokenInfoRes)
{
    AccessTokenIDInner *inner = (AccessTokenIDInner *)&tokenID;
    hapTokenInfoRes.ver = 0;
    hapTokenInfoRes.userID = 100;
    hapTokenInfoRes.instIndex = inner->tokenUniqueID & 1;
    hapTokenInfoRes.deviceID = "54a0a92a428005db27c40bad46bf145fede38ec37effe0347cd990fcb031f320";
    hapTokenInfoRes.tokenID = tokenID;
    if (inner->type == TOKEN_NATIVE) {
        hapTokenInfoRes.apl = APL_SYSTEM_CORE;
        if (inner->tokenUniqueID >= sizeof(NativeName) / sizeof(NativeName[0])) {
            return -1;
        }
        hapTokenInfoRes.bundleName = NativeName[inner->tokenUniqueID % sizeof(NativeName) / sizeof(NativeName[0])];
        hapTokenInfoRes.appID = hapTokenInfoRes.bundleName;
    } else {
        hapTokenInfoRes.apl = APL_NORMAL;
        if (inner->tokenUniqueID >= sizeof(BundleNames) / sizeof(BundleNames[0]) ||
            inner->tokenUniqueID >= sizeof(AppId) / sizeof(AppId[0])) {
            return -1;
        }
        hapTokenInfoRes.bundleName = BundleNames[inner->tokenUniqueID % sizeof(BundleNames) / sizeof(BundleNames[0])];
        hapTokenInfoRes.appID = AppId[inner->tokenUniqueID % sizeof(AppId) / sizeof(AppId[0])];
    }
    return 0;
}

AccessTokenID AccessTokenKit::GetHapTokenID(int userID, const std::string &bundleName, int instIndex)
{
    uint32_t tokenId = 0;
    auto *inner = (AccessTokenIDInner *)&tokenId;
    inner->type = 3;
    for (int i = 0; i < 60; ++i) {
        if (BundleNames[i] == nullptr) {
            break;
        }
        if (bundleName == BundleNames[i]) {
            inner->type = TOKEN_HAP;
            inner->tokenUniqueID = i + 60 * instIndex;
            break;
        }
    }
    return tokenId;
}
AccessTokenIDEx AccessTokenKit::AllocHapToken(const HapInfoParams &info, const HapPolicyParams &policy)
{
    AccessTokenIDEx result;
    AccessTokenIDInner *inner = (AccessTokenIDInner *)&result.tokenIdExStruct.tokenID;
    inner->tokenUniqueID = 0;
    inner->type = TOKEN_HAP;
    return result;
}
int AccessTokenKit::DeleteToken(AccessTokenID tokenID)
{
    return 0;
}

int AccessTokenKit::GrantPermission(AccessTokenID tokenID, const std::string &permissionName, int flag)
{
    return 0;
}
int32_t AccessTokenKit::GetHapDlpFlag(AccessTokenID tokenID)
{
    return 0;
}
int32_t AccessTokenKit::ReloadNativeTokenInfo()
{
    return 0;
}
AccessTokenIDEx AccessTokenKit::GetHapTokenIDEx(int32_t userID, const std::string &bundleName, int32_t instIndex)
{
    AccessTokenIDEx result;
    return result;
}
ATokenTypeEnum AccessTokenKit::GetTokenType(AccessTokenID tokenID)
{
    return TOKEN_HAP;
}
} // namespace OHOS::Security::AccessToken

uint64_t GetAccessTokenId(NativeTokenInfoParams *params)
{
    uint32_t tokenId = 0;
    auto *inner = (OHOS::Security::AccessToken::AccessTokenIDInner *)&tokenId;
    inner->type = 3;
    for (int i = 0; i < 60; ++i) {
        if (NativeName[i] == nullptr) {
            break;
        }
        if (strcmp(NativeName[i], params->processName) == 0) {
            inner->type = OHOS::Security::AccessToken::TOKEN_NATIVE;
            inner->tokenUniqueID = i;
            break;
        }
    }
    return tokenId;
}

int SetSelfTokenID(uint64_t tokenID)
{
    return 0;
}
uint64_t GetSelfTokenID()
{
    return 0;
}