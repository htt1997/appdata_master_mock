/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <innerkits/appexecfwk_base/include/application_info.h>

#include "bundle_info.h"
#include "bundle_mgr_client.h"
#include "bundle_mgr_proxy.h"

namespace OHOS {
namespace AppExecFwk {
BundleMgrClient::BundleMgrClient() {}
BundleMgrClient::~BundleMgrClient() {}

ErrCode BundleMgrClient::GetNameForUid(const int uid, std::string &bundleName)
{
    if (bundleName == "com.huawei.ohos.toteweather") {
        return true;
    }
    return true;
}

bool BundleMgrClient::GetBundleInfo(const std::string &bundleName, const BundleFlag flag, BundleInfo &bundleInfo,
    int32_t userId)
{
    if (bundleName == "com.huawei.ohos.toteweather") {
        bundleInfo.appId = "com.huawei.toteweather_adfasdflaskdfasdf";
        return true;
    }

    if (bundleName == "com.ohos.kvdatamanager.test") {
        return false;
    }
    bundleInfo.appId = bundleName;
    return true;
}

ErrCode BundleMgrClient::GetBundlePackInfo(const std::string &bundleName, const BundlePackFlag flag,
    BundlePackInfo &bundlePackInfo, int32_t userId)
{
    return false;
}

bool BundleMgrClient::GetHapModuleInfo(const std::string &bundleName, const std::string &hapName,
    HapModuleInfo &hapModuleInfo)
{
    return false;
}

bool BundleMgrClient::GetResConfigFile(const HapModuleInfo &hapModuleInfo, const std::string &metadataName,
    std::vector<std::string> &profileInfos) const
{
    return false;
}

bool BundleMgrClient::GetResConfigFile(const ExtensionAbilityInfo &extensionInfo, const std::string &metadataName,
    std::vector<std::string> &profileInfos) const
{
    return false;
}

bool BundleMgrClient::GetResConfigFile(const AbilityInfo &abilityInfo, const std::string &metadataName,
    std::vector<std::string> &profileInfos) const
{
    return false;
}

bool BundleMgrClient::GetProfileFromExtension(const ExtensionAbilityInfo &extensionInfo,
    const std::string &metadataName, std::vector<std::string> &profileInfos) const
{
    return false;
}

bool BundleMgrClient::GetProfileFromAbility(const AbilityInfo &abilityInfo, const std::string &metadataName,
    std::vector<std::string> &profileInfos) const
{
    return false;
}

bool BundleMgrClient::GetProfileFromHap(const HapModuleInfo &hapModuleInfo, const std::string &metadataName,
    std::vector<std::string> &profileInfos) const
{
    return false;
}

ErrCode BundleMgrClient::InstallSandboxApp(const std::string &bundleName, int32_t dlpType, int32_t userId,
    int32_t &appIndex)
{
    return 0;
}

ErrCode BundleMgrClient::UninstallSandboxApp(const std::string &bundleName, int32_t appIndex, int32_t userId)
{
    return 0;
}

ErrCode BundleMgrClient::GetSandboxBundleInfo(const std::string &bundleName, int32_t appIndex, int32_t userId,
    BundleInfo &info)
{
    return 0;
}

ErrCode BundleMgrClient::GetSandboxAbilityInfo(const Want &want, int32_t appIndex, int32_t flags, int32_t userId,
    AbilityInfo &abilityInfo)
{
    return 0;
}

ErrCode BundleMgrClient::GetSandboxExtAbilityInfos(const Want &want, int32_t appIndex, int32_t flags, int32_t userId,
    std::vector<ExtensionAbilityInfo> &extensionInfos)
{
    return 0;
}

ErrCode BundleMgrClient::GetSandboxHapModuleInfo(const AbilityInfo &abilityInfo, int32_t appIndex, int32_t userId,
    HapModuleInfo &hapModuleInfo)
{
    return 0;
}

bool BundleInfo::Marshalling(Parcel &parcel) const
{
    return true;
}
bool ApplicationInfo::Marshalling(Parcel &parcel) const
{
    return true;
}
bool AppExecFwk::Resource::Marshalling(Parcel &parcel) const
{
    return true;
}
BundleMgrProxy::BundleMgrProxy(const sptr<IRemoteObject> &impl) : IRemoteProxy(impl) {}
BundleMgrProxy::~BundleMgrProxy() {}
bool HapModuleInfo::ReadFromParcel(Parcel &parcel)
{
    return false;
}
bool HapModuleInfo::Marshalling(Parcel &parcel) const
{
    return false;
}
HapModuleInfo *HapModuleInfo::Unmarshalling(Parcel &parcel)
{
    return nullptr;
}
bool BundleMgrProxy::GetApplicationInfo(const std::string &appName, const ApplicationFlag flag, const int userId,
    ApplicationInfo &appInfo)
{
    return IBundleMgr::GetApplicationInfo(appName, flag, userId, appInfo);
}
bool BundleMgrProxy::GetApplicationInfo(const std::string &appName, int32_t flags, int32_t userId,
    ApplicationInfo &appInfo)
{
    return IBundleMgr::GetApplicationInfo(appName, flags, userId, appInfo);
}
bool BundleMgrProxy::GetApplicationInfos(const ApplicationFlag flag, int userId, std::vector<ApplicationInfo> &appInfos)
{
    return IBundleMgr::GetApplicationInfos(flag, userId, appInfos);
}
bool BundleMgrProxy::GetApplicationInfos(int32_t flags, int32_t userId, std::vector<ApplicationInfo> &appInfos)
{
    return IBundleMgr::GetApplicationInfos(flags, userId, appInfos);
}
bool BundleMgrProxy::GetBundleInfo(const std::string &bundleName, const BundleFlag flag, BundleInfo &bundleInfo,
    int32_t userId)
{
    return IBundleMgr::GetBundleInfo(bundleName, flag, bundleInfo, userId);
}
bool BundleMgrProxy::GetBundleInfo(const std::string &bundleName, int32_t flags, BundleInfo &bundleInfo, int32_t userId)
{
    return IBundleMgr::GetBundleInfo(bundleName, flags, bundleInfo, userId);
}
ErrCode BundleMgrProxy::GetBundlePackInfo(const std::string &bundleName, const BundlePackFlag flags,
    BundlePackInfo &bundlePackInfo, int32_t userId)
{
    return IBundleMgr::GetBundlePackInfo(bundleName, flags, bundlePackInfo, userId);
}
ErrCode BundleMgrProxy::GetBundlePackInfo(const std::string &bundleName, int32_t flags, BundlePackInfo &bundlePackInfo,
    int32_t userId)
{
    return IBundleMgr::GetBundlePackInfo(bundleName, flags, bundlePackInfo, userId);
}
bool BundleMgrProxy::GetBundleInfos(const BundleFlag flag, std::vector<BundleInfo> &bundleInfos, int32_t userId)
{
    return IBundleMgr::GetBundleInfos(flag, bundleInfos, userId);
}
bool BundleMgrProxy::GetBundleInfos(int32_t flags, std::vector<BundleInfo> &bundleInfos, int32_t userId)
{
    return IBundleMgr::GetBundleInfos(flags, bundleInfos, userId);
}
int BundleMgrProxy::GetUidByBundleName(const std::string &bundleName, const int userId)
{
    return IBundleMgr::GetUidByBundleName(bundleName, userId);
}
std::string BundleMgrProxy::GetAppIdByBundleName(const std::string &bundleName, const int userId)
{
    return IBundleMgr::GetAppIdByBundleName(bundleName, userId);
}
bool BundleMgrProxy::GetBundleNameForUid(const int uid, std::string &bundleName)
{
    return IBundleMgr::GetBundleNameForUid(uid, bundleName);
}
bool BundleMgrProxy::GetBundlesForUid(const int uid, std::vector<std::string> &bundleNames)
{
    return IBundleMgr::GetBundlesForUid(uid, bundleNames);
}
ErrCode BundleMgrProxy::GetNameForUid(const int uid, std::string &name)
{
    return IBundleMgr::GetNameForUid(uid, name);
}
bool BundleMgrProxy::GetBundleGids(const std::string &bundleName, std::vector<int> &gids)
{
    return IBundleMgr::GetBundleGids(bundleName, gids);
}
bool BundleMgrProxy::GetBundleGidsByUid(const std::string &bundleName, const int &uid, std::vector<int> &gids)
{
    return IBundleMgr::GetBundleGidsByUid(bundleName, uid, gids);
}
std::string BundleMgrProxy::GetAppType(const std::string &bundleName)
{
    return IBundleMgr::GetAppType(bundleName);
}
bool BundleMgrProxy::CheckIsSystemAppByUid(const int uid)
{
    return IBundleMgr::CheckIsSystemAppByUid(uid);
}
bool BundleMgrProxy::GetBundleInfosByMetaData(const std::string &metaData, std::vector<BundleInfo> &bundleInfos)
{
    return IBundleMgr::GetBundleInfosByMetaData(metaData, bundleInfos);
}
bool BundleMgrProxy::QueryAbilityInfo(const Want &want, AbilityInfo &abilityInfo)
{
    return IBundleMgr::QueryAbilityInfo(want, abilityInfo);
}
bool BundleMgrProxy::QueryAbilityInfo(const Want &want, int32_t flags, int32_t userId, AbilityInfo &abilityInfo,
    const sptr<IRemoteObject> &callBack)
{
    return IBundleMgr::QueryAbilityInfo(want, flags, userId, abilityInfo, callBack);
}
void BundleMgrProxy::UpgradeAtomicService(const Want &want, int32_t userId)
{
    IBundleMgr::UpgradeAtomicService(want, userId);
}
bool BundleMgrProxy::QueryAbilityInfo(const Want &want, int32_t flags, int32_t userId, AbilityInfo &abilityInfo)
{
    return IBundleMgr::QueryAbilityInfo(want, flags, userId, abilityInfo);
}
bool BundleMgrProxy::QueryAbilityInfos(const Want &want, std::vector<AbilityInfo> &abilityInfos)
{
    return IBundleMgr::QueryAbilityInfos(want, abilityInfos);
}
bool BundleMgrProxy::QueryAbilityInfos(const Want &want, int32_t flags, int32_t userId,
    std::vector<AbilityInfo> &abilityInfos)
{
    return IBundleMgr::QueryAbilityInfos(want, flags, userId, abilityInfos);
}
bool BundleMgrProxy::QueryAllAbilityInfos(const Want &want, int32_t userId, std::vector<AbilityInfo> &abilityInfos)
{
    return IBundleMgr::QueryAllAbilityInfos(want, userId, abilityInfos);
}
bool BundleMgrProxy::QueryAbilityInfoByUri(const std::string &abilityUri, AbilityInfo &abilityInfo)
{
    return IBundleMgr::QueryAbilityInfoByUri(abilityUri, abilityInfo);
}
bool BundleMgrProxy::QueryAbilityInfosByUri(const std::string &abilityUri, std::vector<AbilityInfo> &abilityInfos)
{
    return IBundleMgr::QueryAbilityInfosByUri(abilityUri, abilityInfos);
}
bool BundleMgrProxy::QueryAbilityInfoByUri(const std::string &abilityUri, int32_t userId, AbilityInfo &abilityInfo)
{
    return IBundleMgr::QueryAbilityInfoByUri(abilityUri, userId, abilityInfo);
}
bool BundleMgrProxy::QueryKeepAliveBundleInfos(std::vector<BundleInfo> &bundleInfos)
{
    return IBundleMgr::QueryKeepAliveBundleInfos(bundleInfos);
}
std::string BundleMgrProxy::GetAbilityLabel(const std::string &bundleName, const std::string &abilityName)
{
    return IBundleMgr::GetAbilityLabel(bundleName, abilityName);
}
bool BundleMgrProxy::GetBundleArchiveInfo(const std::string &hapFilePath, const BundleFlag flag, BundleInfo &bundleInfo)
{
    return IBundleMgr::GetBundleArchiveInfo(hapFilePath, flag, bundleInfo);
}
bool BundleMgrProxy::GetBundleArchiveInfo(const std::string &hapFilePath, int32_t flags, BundleInfo &bundleInfo)
{
    return IBundleMgr::GetBundleArchiveInfo(hapFilePath, flags, bundleInfo);
}
bool BundleMgrProxy::GetHapModuleInfo(const AbilityInfo &abilityInfo, HapModuleInfo &hapModuleInfo)
{
    return IBundleMgr::GetHapModuleInfo(abilityInfo, hapModuleInfo);
}
bool BundleMgrProxy::GetHapModuleInfo(const AbilityInfo &abilityInfo, int32_t userId, HapModuleInfo &hapModuleInfo)
{
    return IBundleMgr::GetHapModuleInfo(abilityInfo, userId, hapModuleInfo);
}
bool BundleMgrProxy::CleanBundleDataFiles(const std::string &bundleName, const int userId)
{
    return IBundleMgr::CleanBundleDataFiles(bundleName, userId);
}
bool BundleMgrProxy::RegisterBundleStatusCallback(const sptr<IBundleStatusCallback> &bundleStatusCallback)
{
    return IBundleMgr::RegisterBundleStatusCallback(bundleStatusCallback);
}
bool BundleMgrProxy::ClearBundleStatusCallback(const sptr<IBundleStatusCallback> &bundleStatusCallback)
{
    return IBundleMgr::ClearBundleStatusCallback(bundleStatusCallback);
}
bool BundleMgrProxy::UnregisterBundleStatusCallback()
{
    return IBundleMgr::UnregisterBundleStatusCallback();
}
bool BundleMgrProxy::DumpInfos(const DumpFlag flag, const std::string &bundleName, int32_t userId, std::string &result)
{
    return IBundleMgr::DumpInfos(flag, bundleName, userId, result);
}
sptr<IBundleInstaller> BundleMgrProxy::GetBundleInstaller()
{
    return IBundleMgr::GetBundleInstaller();
}
sptr<IBundleUserMgr> BundleMgrProxy::GetBundleUserMgr()
{
    return IBundleMgr::GetBundleUserMgr();
}
bool BundleMgrProxy::GetAllFormsInfo(std::vector<FormInfo> &formInfos)
{
    return IBundleMgr::GetAllFormsInfo(formInfos);
}
bool BundleMgrProxy::GetFormsInfoByApp(const std::string &bundleName, std::vector<FormInfo> &formInfos)
{
    return IBundleMgr::GetFormsInfoByApp(bundleName, formInfos);
}
bool BundleMgrProxy::GetFormsInfoByModule(const std::string &bundleName, const std::string &moduleName,
    std::vector<FormInfo> &formInfos)
{
    return IBundleMgr::GetFormsInfoByModule(bundleName, moduleName, formInfos);
}
bool BundleMgrProxy::GetShortcutInfos(const std::string &bundleName, std::vector<ShortcutInfo> &shortcutInfos)
{
    return IBundleMgr::GetShortcutInfos(bundleName, shortcutInfos);
}
bool BundleMgrProxy::GetAllCommonEventInfo(const std::string &eventKey, std::vector<CommonEventInfo> &commonEventInfos)
{
    return IBundleMgr::GetAllCommonEventInfo(eventKey, commonEventInfos);
}
bool BundleMgrProxy::GetDistributedBundleInfo(const std::string &networkId, const std::string &bundleName,
    DistributedBundleInfo &distributedBundleInfo)
{
    return IBundleMgr::GetDistributedBundleInfo(networkId, bundleName, distributedBundleInfo);
}
std::string BundleMgrProxy::GetAppPrivilegeLevel(const std::string &bundleName, int32_t userId)
{
    return IBundleMgr::GetAppPrivilegeLevel(bundleName, userId);
}
bool BundleMgrProxy::QueryExtensionAbilityInfos(const Want &want, const int32_t &flag, const int32_t &userId,
    std::vector<ExtensionAbilityInfo> &extensionInfos)
{
    return IBundleMgr::QueryExtensionAbilityInfos(want, flag, userId, extensionInfos);
}
bool BundleMgrProxy::QueryExtensionAbilityInfos(const Want &want, const ExtensionAbilityType &extensionType,
    const int32_t &flag, const int32_t &userId, std::vector<ExtensionAbilityInfo> &extensionInfos)
{
    return IBundleMgr::QueryExtensionAbilityInfos(want, extensionType, flag, userId, extensionInfos);
}
bool BundleMgrProxy::QueryExtensionAbilityInfos(const ExtensionAbilityType &extensionType, const int32_t &userId,
    std::vector<ExtensionAbilityInfo> &extensionInfos)
{
    return IBundleMgr::QueryExtensionAbilityInfos(extensionType, userId, extensionInfos);
}
bool BundleMgrProxy::VerifyCallingPermission(const std::string &permission)
{
    return IBundleMgr::VerifyCallingPermission(permission);
}
bool BundleMgrProxy::QueryExtensionAbilityInfoByUri(const std::string &uri, int32_t userId,
    ExtensionAbilityInfo &extensionAbilityInfo)
{
    return IBundleMgr::QueryExtensionAbilityInfoByUri(uri, userId, extensionAbilityInfo);
}
bool BundleMgrProxy::ImplicitQueryInfoByPriority(const Want &want, int32_t flags, int32_t userId,
    AbilityInfo &abilityInfo, ExtensionAbilityInfo &extensionInfo)
{
    return IBundleMgr::ImplicitQueryInfoByPriority(want, flags, userId, abilityInfo, extensionInfo);
}
bool BundleMgrProxy::GetAbilityInfo(const std::string &bundleName, const std::string &abilityName,
    AbilityInfo &abilityInfo)
{
    return IBundleMgr::GetAbilityInfo(bundleName, abilityName, abilityInfo);
}
bool BundleMgrProxy::GetAbilityInfo(const std::string &bundleName, const std::string &moduleName,
    const std::string &abilityName, AbilityInfo &abilityInfo)
{
    return IBundleMgr::GetAbilityInfo(bundleName, moduleName, abilityName, abilityInfo);
}
ErrCode BundleMgrProxy::GetSandboxBundleInfo(const std::string &bundleName, int32_t appIndex, int32_t userId,
    BundleInfo &info)
{
    return IBundleMgr::GetSandboxBundleInfo(bundleName, appIndex, userId, info);
}
bool BundleMgrProxy::SetModuleRemovable(const std::string &bundleName, const std::string &moduleName, bool isEnable)
{
    return IBundleMgr::SetModuleRemovable(bundleName, moduleName, isEnable);
}
bool BundleMgrProxy::GetAllDependentModuleNames(const std::string &bundleName, const std::string &moduleName,
    std::vector<std::string> &dependentModuleNames)
{
    return IBundleMgr::GetAllDependentModuleNames(bundleName, moduleName, dependentModuleNames);
}
bool BundleMgrProxy::GetModuleUpgradeFlag(const std::string &bundleName, const std::string &moduleName)
{
    return IBundleMgr::GetModuleUpgradeFlag(bundleName, moduleName);
}
bool BundleMgrProxy::ObtainCallingBundleName(std::string &bundleName)
{
    return IBundleMgr::ObtainCallingBundleName(bundleName);
}
bool BundleMgrProxy::GetBundleStats(const std::string &bundleName, int32_t userId, std::vector<int64_t> &bundleStats)
{
    return IBundleMgr::GetBundleStats(bundleName, userId, bundleStats);
}
bool BundleMgrProxy::CheckAbilityEnableInstall(const Want &want, int32_t missionId, int32_t userId,
    const sptr<IRemoteObject> &callback)
{
    return IBundleMgr::CheckAbilityEnableInstall(want, missionId, userId, callback);
}
ErrCode BundleMgrProxy::GetSandboxAbilityInfo(const Want &want, int32_t appIndex, int32_t flags, int32_t userId,
    AbilityInfo &info)
{
    return IBundleMgr::GetSandboxAbilityInfo(want, appIndex, flags, userId, info);
}
ErrCode BundleMgrProxy::GetSandboxExtAbilityInfos(const Want &want, int32_t appIndex, int32_t flags, int32_t userId,
    std::vector<ExtensionAbilityInfo> &infos)
{
    return IBundleMgr::GetSandboxExtAbilityInfos(want, appIndex, flags, userId, infos);
}
ErrCode BundleMgrProxy::GetSandboxHapModuleInfo(const AbilityInfo &abilityInfo, int32_t appIndex, int32_t userId,
    HapModuleInfo &info)
{
    return IBundleMgr::GetSandboxHapModuleInfo(abilityInfo, appIndex, userId, info);
}
ErrCode BundleMgrProxy::GetApplicationInfoV9(const std::string &appName, int32_t flags, int32_t userId,
    ApplicationInfo &appInfo)
{
    return IBundleMgr::GetApplicationInfoV9(appName, flags, userId, appInfo);
}
ErrCode BundleMgrProxy::GetApplicationInfosV9(int32_t flags, int32_t userId, std::vector<ApplicationInfo> &appInfos)
{
    return IBundleMgr::GetApplicationInfosV9(flags, userId, appInfos);
}
ErrCode BundleMgrProxy::GetBundleInfoV9(const std::string &bundleName, int32_t flags, BundleInfo &bundleInfo,
    int32_t userId)
{
    return IBundleMgr::GetBundleInfoV9(bundleName, flags, bundleInfo, userId);
}
ErrCode BundleMgrProxy::BatchGetBundleInfo(const std::vector<Want> &wants, int32_t flags,
    std::vector<BundleInfo> &bundleInfos, int32_t userId)
{
    return IBundleMgr::BatchGetBundleInfo(wants, flags, bundleInfos, userId);
}
ErrCode BundleMgrProxy::BatchGetBundleInfo(const std::vector<std::string> &bundleNames, int32_t flags,
    std::vector<BundleInfo> &bundleInfos, int32_t userId)
{
    return IBundleMgr::BatchGetBundleInfo(bundleNames, flags, bundleInfos, userId);
}
ErrCode BundleMgrProxy::GetBundleInfoForSelf(int32_t flags, BundleInfo &bundleInfo)
{
    return IBundleMgr::GetBundleInfoForSelf(flags, bundleInfo);
}
ErrCode BundleMgrProxy::GetBundleInfosV9(int32_t flags, std::vector<BundleInfo> &bundleInfos, int32_t userId)
{
    return IBundleMgr::GetBundleInfosV9(flags, bundleInfos, userId);
}
int BundleMgrProxy::GetUidByDebugBundleName(const std::string &bundleName, const int userId)
{
    return IBundleMgr::GetUidByDebugBundleName(bundleName, userId);
}
bool BundleMgrProxy::SilentInstall(const Want &want, int32_t userId, const sptr<IRemoteObject> &callBack)
{
    return IBundleMgr::SilentInstall(want, userId, callBack);
}
ErrCode BundleMgrProxy::QueryAbilityInfosV9(const Want &want, int32_t flags, int32_t userId,
    std::vector<AbilityInfo> &abilityInfos)
{
    return IBundleMgr::QueryAbilityInfosV9(want, flags, userId, abilityInfos);
}
ErrCode BundleMgrProxy::BatchQueryAbilityInfos(const std::vector<Want> &wants, int32_t flags, int32_t userId,
    std::vector<AbilityInfo> &abilityInfos)
{
    return IBundleMgr::BatchQueryAbilityInfos(wants, flags, userId, abilityInfos);
}
ErrCode BundleMgrProxy::QueryLauncherAbilityInfos(const Want &want, int32_t userId,
    std::vector<AbilityInfo> &abilityInfo)
{
    return IBundleMgr::QueryLauncherAbilityInfos(want, userId, abilityInfo);
}
ErrCode BundleMgrProxy::GetAbilityLabel(const std::string &bundleName, const std::string &moduleName,
    const std::string &abilityName, std::string &label)
{
    return IBundleMgr::GetAbilityLabel(bundleName, moduleName, abilityName, label);
}
ErrCode BundleMgrProxy::GetBundleArchiveInfoV9(const std::string &hapFilePath, int32_t flags, BundleInfo &bundleInfo)
{
    return IBundleMgr::GetBundleArchiveInfoV9(hapFilePath, flags, bundleInfo);
}
ErrCode BundleMgrProxy::GetLaunchWantForBundle(const std::string &bundleName, Want &want, int32_t userId)
{
    return IBundleMgr::GetLaunchWantForBundle(bundleName, want, userId);
}
ErrCode BundleMgrProxy::CleanBundleCacheFiles(const std::string &bundleName,
    const sptr<ICleanCacheCallback> cleanCacheCallback, int32_t userId)
{
    return IBundleMgr::CleanBundleCacheFiles(bundleName, cleanCacheCallback, userId);
}
bool BundleMgrProxy::RegisterBundleEventCallback(const sptr<IBundleEventCallback> &bundleEventCallback)
{
    return IBundleMgr::RegisterBundleEventCallback(bundleEventCallback);
}
bool BundleMgrProxy::UnregisterBundleEventCallback(const sptr<IBundleEventCallback> &bundleEventCallback)
{
    return IBundleMgr::UnregisterBundleEventCallback(bundleEventCallback);
}
ErrCode BundleMgrProxy::CompileProcessAOT(const std::string &bundleName, const std::string &compileMode,
    bool isAllBundle)
{
    return IBundleMgr::CompileProcessAOT(bundleName, compileMode, isAllBundle);
}
ErrCode BundleMgrProxy::CompileReset(const std::string &bundleName, bool isAllBundle)
{
    return IBundleMgr::CompileReset(bundleName, isAllBundle);
}
ErrCode BundleMgrProxy::IsApplicationEnabled(const std::string &bundleName, bool &isEnable)
{
    return IBundleMgr::IsApplicationEnabled(bundleName, isEnable);
}
ErrCode BundleMgrProxy::IsAbilityEnabled(const AbilityInfo &abilityInfo, bool &isEnable)
{
    return IBundleMgr::IsAbilityEnabled(abilityInfo, isEnable);
}
ErrCode BundleMgrProxy::GetShortcutInfoV9(const std::string &bundleName, std::vector<ShortcutInfo> &shortcutInfos)
{
    return IBundleMgr::GetShortcutInfoV9(bundleName, shortcutInfos);
}
ErrCode BundleMgrProxy::QueryExtensionAbilityInfosV9(const Want &want, int32_t flags, int32_t userId,
    std::vector<ExtensionAbilityInfo> &extensionInfos)
{
    return IBundleMgr::QueryExtensionAbilityInfosV9(want, flags, userId, extensionInfos);
}
ErrCode BundleMgrProxy::QueryExtensionAbilityInfosV9(const Want &want, const ExtensionAbilityType &extensionType,
    int32_t flags, int32_t userId, std::vector<ExtensionAbilityInfo> &extensionInfos)
{
    return IBundleMgr::QueryExtensionAbilityInfosV9(want, extensionType, flags, userId, extensionInfos);
}
bool BundleMgrProxy::ImplicitQueryInfos(const Want &want, int32_t flags, int32_t userId, bool withDefault,
    std::vector<AbilityInfo> &abilityInfos, std::vector<ExtensionAbilityInfo> &extensionInfos)
{
    return IBundleMgr::ImplicitQueryInfos(want, flags, userId, withDefault, abilityInfos, extensionInfos);
}
ErrCode BundleMgrProxy::IsModuleRemovable(const std::string &bundleName, const std::string &moduleName,
    bool &isRemovable)
{
    return IBundleMgr::IsModuleRemovable(bundleName, moduleName, isRemovable);
}
bool BundleMgrProxy::ProcessPreload(const Want &want)
{
    return IBundleMgr::ProcessPreload(want);
}
bool BundleMgrProxy::GetAllBundleStats(int32_t userId, std::vector<int64_t> &bundleStats)
{
    return IBundleMgr::GetAllBundleStats(userId, bundleStats);
}
ErrCode BundleMgrProxy::GetMediaData(const std::string &bundleName, const std::string &moduleName,
    const std::string &abilityName, std::unique_ptr<uint8_t[]> &mediaDataPtr, size_t &len, int32_t userId)
{
    return IBundleMgr::GetMediaData(bundleName, moduleName, abilityName, mediaDataPtr, len, userId);
}
std::string BundleMgrProxy::GetStringById(const std::string &bundleName, const std::string &moduleName, uint32_t resId,
    int32_t userId, const std::string &localeInfo)
{
    return IBundleMgr::GetStringById(bundleName, moduleName, resId, userId, localeInfo);
}
std::string BundleMgrProxy::GetIconById(const std::string &bundleName, const std::string &moduleName, uint32_t resId,
    uint32_t density, int32_t userId)
{
    return IBundleMgr::GetIconById(bundleName, moduleName, resId, density, userId);
}
ErrCode BundleMgrProxy::GetAllProxyDataInfos(std::vector<ProxyData> &proxyDatas, int32_t userId)
{
    return IBundleMgr::GetAllProxyDataInfos(proxyDatas, userId);
}
ErrCode BundleMgrProxy::GetProxyDataInfos(const std::string &bundleName, const std::string &moduleName,
    std::vector<ProxyData> &proxyDatas, int32_t userId)
{
    return IBundleMgr::GetProxyDataInfos(bundleName, moduleName, proxyDatas, userId);
}
ErrCode BundleMgrProxy::GetSpecifiedDistributionType(const std::string &bundleName,
    std::string &specifiedDistributionType)
{
    return IBundleMgr::GetSpecifiedDistributionType(bundleName, specifiedDistributionType);
}
ErrCode BundleMgrProxy::GetAdditionalInfo(const std::string &bundleName, std::string &additionalInfo)
{
    return IBundleMgr::GetAdditionalInfo(bundleName, additionalInfo);
}
ErrCode BundleMgrProxy::SetExtNameOrMIMEToApp(const std::string &bundleName, const std::string &moduleName,
    const std::string &abilityName, const std::string &extName, const std::string &mimeType)
{
    return IBundleMgr::SetExtNameOrMIMEToApp(bundleName, moduleName, abilityName, extName, mimeType);
}
ErrCode BundleMgrProxy::DelExtNameOrMIMEToApp(const std::string &bundleName, const std::string &moduleName,
    const std::string &abilityName, const std::string &extName, const std::string &mimeType)
{
    return IBundleMgr::DelExtNameOrMIMEToApp(bundleName, moduleName, abilityName, extName, mimeType);
}
bool BundleMgrProxy::GetGroupDir(const std::string &dataGroupId, std::string &dir)
{
    return IBundleMgr::GetGroupDir(dataGroupId, dir);
}
bool BundleMgrProxy::QueryAppGalleryBundleName(std::string &bundleName)
{
    return IBundleMgr::QueryAppGalleryBundleName(bundleName);
}
ErrCode BundleMgrProxy::QueryExtensionAbilityInfosWithTypeName(const Want &want, const std::string &extensionTypeName,
    const int32_t flag, const int32_t userId, std::vector<ExtensionAbilityInfo> &extensionInfos)
{
    return IBundleMgr::QueryExtensionAbilityInfosWithTypeName(want, extensionTypeName, flag, userId, extensionInfos);
}
ErrCode BundleMgrProxy::QueryExtensionAbilityInfosOnlyWithTypeName(const std::string &extensionTypeName,
    const uint32_t flag, const int32_t userId, std::vector<ExtensionAbilityInfo> &extensionInfos)
{
    return IBundleMgr::QueryExtensionAbilityInfosOnlyWithTypeName(extensionTypeName, flag, userId, extensionInfos);
}
ErrCode BundleMgrProxy::ResetAOTCompileStatus(const std::string &bundleName, const std::string &moduleName,
    int32_t triggerMode)
{
    return IBundleMgr::ResetAOTCompileStatus(bundleName, moduleName, triggerMode);
}
ErrCode BundleMgrProxy::GetJsonProfile(ProfileType profileType, const std::string &bundleName,
    const std::string &moduleName, std::string &profile, int32_t userId)
{
    return IBundleMgr::GetJsonProfile(profileType, bundleName, moduleName, profile, userId);
}
ErrCode BundleMgrProxy::GetUninstalledBundleInfo(const std::string bundleName, BundleInfo &bundleInfo)
{
    return IBundleMgr::GetUninstalledBundleInfo(bundleName, bundleInfo);
}
ErrCode BundleMgrProxy::SetAdditionalInfo(const std::string &bundleName, const std::string &additionalInfo)
{
    return IBundleMgr::SetAdditionalInfo(bundleName, additionalInfo);
}
ErrCode BundleMgrProxy::CreateBundleDataDir(int32_t userId)
{
    return IBundleMgr::CreateBundleDataDir(userId);
}
ErrCode BundleMgrProxy::CanOpenLink(const std::string &link, bool &canOpen)
{
    return IBundleMgr::CanOpenLink(link, canOpen);
}
ErrCode BundleMgrProxy::GetOdid(std::string &odid)
{
    return IBundleMgr::GetOdid(odid);
}
ErrCode BundleMgrProxy::GetAllBundleInfoByDeveloperId(const std::string &developerId,
    std::vector<BundleInfo> &bundleInfos, int32_t userId)
{
    return IBundleMgr::GetAllBundleInfoByDeveloperId(developerId, bundleInfos, userId);
}
ErrCode BundleMgrProxy::GetDeveloperIds(const std::string &appDistributionType,
    std::vector<std::string> &developerIdList, int32_t userId)
{
    return IBundleMgr::GetDeveloperIds(appDistributionType, developerIdList, userId);
}
ErrCode BundleMgrProxy::SwitchUninstallState(const std::string &bundleName, const bool &state)
{
    return IBundleMgr::SwitchUninstallState(bundleName, state);
}
ErrCode BundleMgrProxy::QueryAbilityInfoByContinueType(const std::string &bundleName, const std::string &continueType,
    AbilityInfo &abilityInfo, int32_t userId)
{
    return IBundleMgr::QueryAbilityInfoByContinueType(bundleName, continueType, abilityInfo, userId);
}
ErrCode BundleMgrProxy::QueryCloneAbilityInfo(const ElementName &element, int32_t flags, int32_t appIndex,
    AbilityInfo &abilityInfo, int32_t userId)
{
    return IBundleMgr::QueryCloneAbilityInfo(element, flags, appIndex, abilityInfo, userId);
}
ErrCode BundleMgrProxy::GetCloneBundleInfo(const std::string &bundleName, int32_t flag, int32_t appIndex,
    BundleInfo &bundleInfo, int32_t userId)
{
    return IBundleMgr::GetCloneBundleInfo(bundleName, flag, appIndex, bundleInfo, userId);
}
ErrCode BundleMgrProxy::GetPermissionDef(const std::string &permissionName, PermissionDef &permissionDef)
{
    return IBundleMgr::GetPermissionDef(permissionName, permissionDef);
}
ErrCode BundleMgrProxy::SetApplicationEnabled(const std::string &bundleName, bool isEnable, int32_t userId)
{
    return IBundleMgr::SetApplicationEnabled(bundleName, isEnable, userId);
}
ErrCode BundleMgrProxy::SetAbilityEnabled(const AbilityInfo &abilityInfo, bool isEnabled, int32_t userId)
{
    return IBundleMgr::SetAbilityEnabled(abilityInfo, isEnabled, userId);
}
ErrCode BundleMgrProxy::SetModuleUpgradeFlag(const std::string &bundleName, const std::string &moduleName,
    int32_t upgradeFlag)
{
    return IBundleMgr::SetModuleUpgradeFlag(bundleName, moduleName, upgradeFlag);
}
Metadata::Metadata(const std::string &paramName, const std::string &paramValue, const std::string &paramResource) {}
bool Metadata::ReadFromParcel(Parcel &parcel)
{
    return false;
}
bool Metadata::Marshalling(Parcel &parcel) const
{
    return false;
}
Metadata *Metadata::Unmarshalling(Parcel &parcel)
{
    return nullptr;
}
CustomizeData::CustomizeData(std::string paramName, std::string paramValue, std::string paramExtra) {}
bool CustomizeData::ReadFromParcel(Parcel &parcel)
{
    return false;
}
bool CustomizeData::Marshalling(Parcel &parcel) const
{
    return false;
}
CustomizeData *CustomizeData::Unmarshalling(Parcel &parcel)
{
    return nullptr;
}
bool RequestPermissionUsedScene::ReadFromParcel(Parcel &parcel)
{
    return false;
}
bool RequestPermissionUsedScene::Marshalling(Parcel &parcel) const
{
    return false;
}
RequestPermissionUsedScene *RequestPermissionUsedScene::Unmarshalling(Parcel &parcel)
{
    return nullptr;
}
bool RequestPermission::ReadFromParcel(Parcel &parcel)
{
    return false;
}
bool RequestPermission::Marshalling(Parcel &parcel) const
{
    return false;
}
RequestPermission *RequestPermission::Unmarshalling(Parcel &parcel)
{
    return nullptr;
}
bool ModuleInfo::ReadFromParcel(Parcel &parcel)
{
    return false;
}
bool ModuleInfo::Marshalling(Parcel &parcel) const
{
    return false;
}
ModuleInfo *ModuleInfo::Unmarshalling(Parcel &parcel)
{
    return nullptr;
}
bool SignatureInfo::ReadFromParcel(Parcel &parcel)
{
    return false;
}
bool SignatureInfo::Marshalling(Parcel &parcel) const
{
    return false;
}
SignatureInfo *SignatureInfo::Unmarshalling(Parcel &parcel)
{
    return nullptr;
}
bool SimpleAppInfo::ReadFromParcel(Parcel &parcel)
{
    return false;
}
bool SimpleAppInfo::Marshalling(Parcel &parcel) const
{
    return false;
}
SimpleAppInfo *SimpleAppInfo::Unmarshalling(Parcel &parcel)
{
    return nullptr;
}
std::string SimpleAppInfo::ToString() const
{
    return std::string();
}
} // namespace AppExecFwk
} // namespace OHOS