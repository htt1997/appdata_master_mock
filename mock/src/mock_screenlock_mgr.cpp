/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "screenlock_manager.h"

namespace OHOS::ScreenLock {
sptr<ScreenLockManager> ScreenLockManager::GetInstance()
{
    static sptr<ScreenLockManager> instance = new ScreenLockManager;
    return instance;
}

int32_t ScreenLockManager::Lock(int32_t userId)
{
    return 0;
}
int32_t ScreenLockManager::IsLocked(bool &isLocked)
{
    return 0;
}
bool ScreenLockManager::IsScreenLocked()
{
    return false;
}
bool ScreenLockManager::GetSecure()
{
    return false;
}
int32_t ScreenLockManager::Unlock(Action action, const sptr<ScreenLockCallbackInterface> &listener)
{
    return 0;
}
int32_t ScreenLockManager::Lock(const sptr<ScreenLockCallbackInterface> &listener)
{
    return 0;
}
} // namespace OHOS::ScreenLock