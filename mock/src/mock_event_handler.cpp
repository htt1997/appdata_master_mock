/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "event_handler.h"
namespace OHOS::AppExecFwk {
bool EventHandler::SendEvent(InnerEvent::Pointer &event, int64_t delayTime, Priority priority) { return false; }
EventHandler::EventHandler(const std::shared_ptr<EventRunner> &runner)
{
}
EventHandler::~EventHandler()
{
}
void EventHandler::ProcessEvent(const InnerEvent::Pointer &event)
{
}
std::shared_ptr<EventHandler> EventHandler::Current()
{
    return std::shared_ptr<EventHandler>();
}
InnerEvent::Pointer InnerEvent::Get(const InnerEvent::Callback &callback, const std::string &name)
{
    return InnerEvent::Pointer(nullptr, nullptr);
}
std::shared_ptr<EventRunner> EventRunner::Create(const std::string &threadName)
{
    return std::shared_ptr<EventRunner>();
}
}