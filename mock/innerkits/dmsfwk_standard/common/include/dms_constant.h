/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef OHOS_DISTRIBUTED_DMS_CONSTANT_H
#define OHOS_DISTRIBUTED_DMS_CONSTANT_H

namespace OHOS {
namespace DistributedSchedule {
namespace Constants {
constexpr const char* DMS_NAME = "dmsfwk";
constexpr const char* DMS_VERSION = "4.1.0";
constexpr const char* DMS_SERVICE_ID = "dmsfwk_svr_id";
constexpr const char* DMS_CHAR_ID = "dmsInfo";
constexpr const char* PACKAGE_NAMES = "packageNames";
constexpr const char* VERSIONS = "versions";
constexpr const char* DMS_SERVICE_TYPE = "appInfo";
} // Constants
} // DistributedSchedule
} // OHOS
#endif
