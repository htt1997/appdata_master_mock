/*
* Copyright (c) 2024 Huawei Device Co., Ltd.
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#ifndef OS_ACCOUNT_FRAMEWORKS_OSACCOUNT_CORE_INCLUDE_OS_ACCOUNT_STATE_REPLY_CALLBACK_H
#define OS_ACCOUNT_FRAMEWORKS_OSACCOUNT_CORE_INCLUDE_OS_ACCOUNT_STATE_REPLY_CALLBACK_H

#include "iremote_proxy.h"
#include <iremote_stub.h>

namespace OHOS {
namespace AccountSA {
class IOsAccountStateReplyCallback : public IRemoteBroker {
public:
    DECLARE_INTERFACE_DESCRIPTOR(u"ohos.accountfwk.IOsAccountStateReplyCallback");

    virtual void OnComplete() = 0;
};

class OsAccountStateReplyCallback : public IRemoteProxy<IOsAccountStateReplyCallback> {
public:
    explicit OsAccountStateReplyCallback(const sptr<IRemoteObject> &object) : IRemoteProxy(object) {}
    ~OsAccountStateReplyCallback() override = default;

    void OnComplete() override {}

private:
    static inline BrokerDelegator<OsAccountStateReplyCallback> delegator_;
};
}  // namespace AccountSA
}  // namespace OHOS
#endif  // OS_ACCOUNT_FRAMEWORKS_OSACCOUNT_CORE_INCLUDE_OS_ACCOUNT_STATE_REPLY_CALLBACK_H
